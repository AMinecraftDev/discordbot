package net.aminecraftdev.spigot.api.exceptions;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class SpamWarningException extends RuntimeException {
    private static final long serialVersionUID = 854651688L;
}
