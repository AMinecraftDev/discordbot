package net.aminecraftdev.spigot.api.exceptions;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class ConnectionFailedException extends Exception {
    private static final long serialVersionUID = 648468748L;

}
