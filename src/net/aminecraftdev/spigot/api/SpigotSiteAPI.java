package net.aminecraftdev.spigot.api;


import net.aminecraftdev.spigot.api.forum.ForumManager;
import net.aminecraftdev.spigot.api.resource.ResourceManager;
import net.aminecraftdev.spigot.api.user.ConversationManager;
import net.aminecraftdev.spigot.api.user.UserManager;

/**
 * Spigot Site Application Programmable Interface
 *
 * @author Maxim Van de Wynckel
 */
public interface SpigotSiteAPI {
    /**
     * Get spigot user manager
     *
     * @return {@link net.aminecraftdev.spigot.api.user.UserManager}
     */
    UserManager getUserManager();

    /**
     * Get spigot resource manager
     *
     * @return {@link net.aminecraftdev.spigot.api.resource.ResourceManager}
     */
    ResourceManager getResourceManager();

    /**
     * Get spigot forum manager
     *
     * @return {@link net.aminecraftdev.spigot.api.forum.ForumManager}
     */
    ForumManager getForumManager();

    /**
     * Get spigot conversation manager
     *
     * @return {@link net.aminecraftdev.spigot.api.user.ConversationManager}
     */
    ConversationManager getConversationManager();
}