package net.aminecraftdev.spigot.api.forum;

import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;

/**
 * Spigot forum manager
 *
 * @author Maxim Van de Wynckel
 */
public interface ForumManager {
    /**
     * Get forum by identifier
     *
     * @param id Forum id
     */
    Forum getForumById(int id) throws ConnectionFailedException;
}