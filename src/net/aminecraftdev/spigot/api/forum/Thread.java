package net.aminecraftdev.spigot.api.forum;

import net.aminecraftdev.spigot.api.user.User;

import java.util.List;

/**
 * Spigot forum thread
 *
 * @author Maxim Van de Wynckel
 */
public interface Thread {
    /**
     * Get thread replies
     *
     * @return List of {@link net.aminecraftdev.spigot.api.forum.Post}
     */
    List<Post> getReplies();

    /**
     * Get original post
     *
     * @return {@link net.aminecraftdev.spigot.api.forum.Post}
     */
    Post getOriginalPost();

    /**
     * Get thread creator
     *
     * @return Thread creator
     */
    User getCreator();
}