package net.aminecraftdev.spigot.api.forum;

import java.util.List;

/**
 * Spigot forum category
 *
 * @author Maxim Van de Wynckel
 */
public interface ForumCategory {
    /**
     * Get forums inside category
     */
    List<Forum> getForums();
}