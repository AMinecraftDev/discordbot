package net.aminecraftdev.spigot.api.forum;

import net.aminecraftdev.spigot.api.user.User;

/**
 * Forum thread reply
 *
 * @author Maxim Van de Wynckel
 */
public interface Post {
    /**
     * Get the author of the reply
     *
     * @return Spigot User
     */
    User getAuthor();
}