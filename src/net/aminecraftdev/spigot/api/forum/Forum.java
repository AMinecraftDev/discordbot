package net.aminecraftdev.spigot.api.forum;

import java.util.List;

/**
 * Spigot forum
 *
 * @author Maxim Van de Wynckel
 */
public interface Forum {
    /**
     * Get sub forums
     */
    List<Forum> getSubForums();
}