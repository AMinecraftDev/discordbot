package net.aminecraftdev.spigot.site.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class StringUtils {

    public static String getStringBetween(String source, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(source);
        while (matcher.find()) {
            return matcher.group(1);
        }
        return "";
    }

}
