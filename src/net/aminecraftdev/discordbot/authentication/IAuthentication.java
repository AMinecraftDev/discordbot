package net.aminecraftdev.discordbot.authentication;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IAuthentication  {

    String get2FAKey();

    void set2FAKey(String string);

    String getPassword();

    void setPassword(String string);

    String getUsername();

    void setUsername(String string);

}
