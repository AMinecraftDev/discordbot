package net.aminecraftdev.discordbot.authentication;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class Authentication implements IAuthentication {

    private String domain, password, username;

    @Override
    public String get2FAKey() {
        return this.domain;
    }

    @Override
    public void set2FAKey(String string) {
        this.domain = string;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String string) {
        this.password = string;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public void setUsername(String string) {
        this.username = string;
    }





}
