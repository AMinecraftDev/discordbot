package net.aminecraftdev.discordbot.authentication.factory;

import net.aminecraftdev.discordbot.authentication.IAuthentication;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class AuthenticationFactory implements IAuthenticationFactory {

    private IAuthentication _product;

    public AuthenticationFactory(IAuthentication product) {
        _product = product;
    }

    @Override
    public IAuthentication getInstance() {
        try {
            return _product.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
