package net.aminecraftdev.discordbot.authentication.factory;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.factory.IFactory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IAuthenticationFactory extends IFactory<IAuthentication> {

}
