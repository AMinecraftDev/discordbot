package net.aminecraftdev.discordbot.database.interfaces;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public interface IDatabaseValues {

    List<String> getAllColumnLabels();

    String getTableName();

}
