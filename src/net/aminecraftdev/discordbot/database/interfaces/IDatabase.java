package net.aminecraftdev.discordbot.database.interfaces;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface IDatabase {

    void connect();

    void disconnect();

    Connection getConnection() throws SQLException;

    ResultSet executeQuery(boolean hideErrors, String sql, Object... objects);

    ResultSet executeQuery(String sql, Object... objects);

    void executeUpdate(boolean hideErrors, String sql, Object... objects);

    void executeUpdate(String sql, Object... objects);

    void createTable(String tableName, List<String> columns);

    String selectFrom(String select, String from, String row, IDatabaseValues databaseValues);

    ResultSet selectAllFrom(String from, String row, IDatabaseValues databaseValues);

    void deleteFrom(String from, String row, IDatabaseValues databaseValues);

    boolean doesExist(String column, String row, IDatabaseValues databaseValues);

}
