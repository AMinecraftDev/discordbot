package net.aminecraftdev.discordbot.database.users;

import net.aminecraftdev.discordbot.database.interfaces.IDatabaseValues;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public interface IUserDatabaseValues extends IDatabaseValues {

    String getDiscordId();

    String getVerifiedDate();

    String getSpigotId();

    String getGeneratedKey();

}
