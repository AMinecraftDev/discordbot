package net.aminecraftdev.discordbot.database.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public class UserDatabaseValues implements IUserDatabaseValues {

    @Override
    public List<String> getAllColumnLabels() {
        return new ArrayList<>(Arrays.asList(
                getDiscordId() + " BIGINT NOT NULL",
                getSpigotId() + " BIGINT NOT NULL",
                getVerifiedDate() + " BIGINT NOT NULL",
                getGeneratedKey() + " VARCHAR(32)",
                "PRIMARY KEY (" + getDiscordId() + ")")
        );
    }

    @Override
    public String getDiscordId() {
        return "discordId";
    }

    @Override
    public String getVerifiedDate() {
        return "verifiedDate";
    }

    @Override
    public String getSpigotId() {
        return "spigotId";
    }

    @Override
    public String getGeneratedKey() {
        return "generatedKey";
    }

    @Override
    public String getTableName() {
        return "aminerix";
    }
}
