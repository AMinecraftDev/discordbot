package net.aminecraftdev.discordbot.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.authentication.IDatabaseAuthentication;
import net.aminecraftdev.discordbot.database.interfaces.IDatabaseValues;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class HikariCP implements IDatabase {

    private final IDatabaseAuthentication databaseAuthentication;

    private HikariDataSource hikariDataSource;

    public HikariCP(IDatabaseAuthentication databaseAuthentication) {
        this.databaseAuthentication = databaseAuthentication;
    }

    @Override
    public void connect() {
        try {
            HikariConfig hikariConfig = new HikariConfig();

            hikariConfig.setJdbcUrl("jdbc:mysql://" + this.databaseAuthentication.getHost() + ":" + this.databaseAuthentication.getPort() + "/" + this.databaseAuthentication.getDatabase() + "?characterEncoding=utf8");
            hikariConfig.setUsername(this.databaseAuthentication.getUsername());
            hikariConfig.setPassword(this.databaseAuthentication.getPassword());
            hikariConfig.setMinimumIdle(0);
            hikariConfig.setMaximumPoolSize(30);

            hikariConfig.addDataSourceProperty("useSSL", false);
            hikariConfig.addDataSourceProperty("cachePrepStmts", true);
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", 250);
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
            hikariConfig.addDataSourceProperty("useServerPrepStmts", true);
            hikariConfig.addDataSourceProperty("useLocalSessionState", true);
            hikariConfig.addDataSourceProperty("useLocalTransactionState", true);
            hikariConfig.addDataSourceProperty("rewriteBatchedStatements", true);
            hikariConfig.addDataSourceProperty("cacheResultSetMetadata", true);
            hikariConfig.addDataSourceProperty("cacheServerConfiguration", true);
            hikariConfig.addDataSourceProperty("elideSetAutoCommits", true);
            hikariConfig.addDataSourceProperty("maintainTimeStats", false);

            hikariConfig.setIdleTimeout(60000);
            hikariConfig.setConnectionTimeout(60000);
            hikariConfig.setValidationTimeout(3000);

            this.hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
        this.hikariDataSource.close();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return this.hikariDataSource.getConnection();
    }

    @Override
    public ResultSet executeQuery(boolean hideErrors, String sql, Object... objects) {
        try(Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            int count = 1;

            for(Object object : objects) {
                preparedStatement.setObject(count, object);
                count++;
            }

            return preparedStatement.executeQuery();
        } catch (SQLException ex) {
            if(!hideErrors) {
                ex.printStackTrace();
            }
        }

        return null;
    }

    @Override
    public ResultSet executeQuery(String sql, Object... objects) {
        return executeQuery(false, sql, objects);
    }

    @Override
    public void executeUpdate(boolean hideErrors, String sql, Object... objects) {
        try(Connection connection = getConnection(); PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            int count = 1;

            for(Object object : objects) {
                preparedStatement.setObject(count, object);
                count++;
            }

            preparedStatement.execute();
        } catch (SQLException ex) {
            if(!hideErrors) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void executeUpdate(String sql, Object... objects) {
        executeUpdate(false, sql, objects);
    }

    @Override
    public void createTable(String tableName, List<String> columns) {
        StringBuilder stringBuilder = new StringBuilder("(");
        Queue<String> queue = new LinkedList<>(columns);

        while(!queue.isEmpty()) {
            stringBuilder.append(queue.poll());

            if(!queue.isEmpty()) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append(");");

        executeUpdate("CREATE TABLE IF NOT EXISTS " + tableName + " " + stringBuilder.toString());
    }

    @Override
    public String selectFrom(String select, String from, String row, IDatabaseValues databaseValues) {
        ResultSet resultSet = executeQuery("SELECT " + select + " FROM " + databaseValues.getTableName() + " WHERE " + from + "=?;", row);

        try {
            return resultSet.getString(1);
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public ResultSet selectAllFrom(String from, String row, IDatabaseValues databaseValues) {
        ResultSet resultSet = executeQuery("SELECT * FROM " + databaseValues.getTableName() + " WHERE " + from + "=?;", row);

        try {
            resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultSet;
    }

    @Override
    public void deleteFrom(String from, String row, IDatabaseValues databaseValues) {
        executeUpdate("DELETE FROM " + databaseValues.getTableName() + " WHERE " + from + "=?;", row);
    }

    @Override
    public boolean doesExist(String column, String row, IDatabaseValues databaseValues) {
        ResultSet resultSet = executeQuery("SELECT * FROM " + databaseValues.getTableName() + " WHERE " + column + "=?;", row);

        try {
            return resultSet.next();
        } catch (SQLException e) {
            return false;
        }
    }
}
