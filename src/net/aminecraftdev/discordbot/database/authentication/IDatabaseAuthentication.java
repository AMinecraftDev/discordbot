package net.aminecraftdev.discordbot.database.authentication;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface IDatabaseAuthentication {

    String getHost();

    void setHost(String host);

    String getPort();

    void setPort(String port);

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getDatabase();

    void setDatabase(String database);

}
