package net.aminecraftdev.discordbot.database.authentication;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class DatabaseAuthentication implements IDatabaseAuthentication {

    private String host, port, username, password, database;

    @Override
    public String getHost() {
        return this.host;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String getPort() {
        return this.port;
    }

    @Override
    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getDatabase() {
        return this.database;
    }

    @Override
    public void setDatabase(String database) {
        this.database = database;
    }
}
