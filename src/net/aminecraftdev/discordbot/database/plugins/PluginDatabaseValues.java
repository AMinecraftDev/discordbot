package net.aminecraftdev.discordbot.database.plugins;

import java.util.*;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public class PluginDatabaseValues implements IPluginDatabaseValues {

    @Override
    public String getPluginId() {
        return "pluginId";
    }

    @Override
    public String getPluginName() {
        return "pluginName";
    }

    @Override
    public String getAuthorId() {
        return "authorId";
    }

    @Override
    public String getUpdateChannelId() {
        return "updateChannelId";
    }

    @Override
    public String getPremium() {
        return "premium";
    }

    @Override
    public String getCost() {
        return "cost";
    }

    @Override
    public List<String> getAllColumnLabels() {
        return new ArrayList<>(Arrays.asList(
                getPluginId() + " BIGINT NOT NULL",
                getPluginName() + " VARCHAR(32)",
                getAuthorId() + " BIGINT NOT NULL",
                getPremium() + " BOOLEAN NOT NULL",
                getCost() + " INT",
                getUpdateChannelId() + " BIGINT NOT NULL",
                "PRIMARY KEY (" + getPluginId() + ")")
        );
    }

    @Override
    public String getTableName() {
        return "plugins";
    }

    @Override
    public String getInsertString(String pluginName, int pluginId, boolean premium, Double cost, long authorId, long updateChannelId) {
        StringBuilder columnBuilder = new StringBuilder("(");
        StringBuilder valuesBuilder = new StringBuilder("(");
        StringBuilder onDuplicateBuilder = new StringBuilder();

        Map<String, Object> objectMap = new HashMap<>();

        objectMap.put(getPluginName(), pluginName);
        objectMap.put(getAuthorId(), authorId);
        objectMap.put(getPremium(), premium);
        objectMap.put(getUpdateChannelId(), updateChannelId);
        objectMap.put(getCost(), cost);

        columnBuilder.append("`").append(getPluginId()).append("`");
        columnBuilder.append(", ");

        valuesBuilder.append(pluginId);
        valuesBuilder.append(", ");

        Iterator<Map.Entry<String, Object>> iterator = objectMap.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String string = entry.getKey();
            Object value = entry.getValue();

            columnBuilder.append("`");
            columnBuilder.append(string);
            columnBuilder.append("`");
            checkIfString(value, valuesBuilder);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append(string);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append("=");
            checkIfString(value, onDuplicateBuilder);

            if(iterator.hasNext()) {
                columnBuilder.append(", ");
                valuesBuilder.append(", ");
                onDuplicateBuilder.append(", ");
            }
        }

        columnBuilder.append(")");
        valuesBuilder.append(")");

        String value = "INSERT INTO " + getTableName() + " " + columnBuilder.toString() + " VALUES " + valuesBuilder.toString() + " ON DUPLICATE KEY UPDATE " + onDuplicateBuilder.toString() + ";";

//        System.out.println("Database value: " + value);

        return value;
    }

    private void checkIfString(Object value, StringBuilder stringBuilder) {
        if(value instanceof String) {
            stringBuilder.append("\"");
            stringBuilder.append(value);
            stringBuilder.append("\"");
        } else {
            stringBuilder.append(value);
        }
    }
}
