package net.aminecraftdev.discordbot.database.plugins;

import net.aminecraftdev.discordbot.database.interfaces.IDatabaseValues;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public interface IPluginDatabaseValues extends IDatabaseValues {

    String getPluginId();

    String getPluginName();

    String getAuthorId();

    String getUpdateChannelId();

    String getPremium();

    String getCost();

    String getInsertString(String pluginName, int pluginId, boolean premium, Double cost, long authorId, long updateChannelId);
}
