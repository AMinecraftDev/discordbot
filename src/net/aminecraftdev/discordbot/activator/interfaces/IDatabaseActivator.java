package net.aminecraftdev.discordbot.activator.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface IDatabaseActivator extends IActivator {
}
