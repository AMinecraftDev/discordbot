package net.aminecraftdev.discordbot.activator;

import net.aminecraftdev.discordbot.activator.interfaces.IDatabaseActivator;
import net.aminecraftdev.discordbot.database.authentication.IDatabaseAuthentication;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.database.users.IUserDatabaseValues;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class DatabaseActivator implements IDatabaseActivator {

    private IPluginDatabaseValues pluginDatabaseValues;
    private IUserDatabaseValues userDatabaseValues;

    private IDatabaseAuthentication databaseAuthentication;
    private IDatabase database;

    public DatabaseActivator(IDatabaseAuthentication databaseAuthentication, IDatabase database, IPluginDatabaseValues pluginDatabaseValues, IUserDatabaseValues userDatabaseValues) {
        this.databaseAuthentication = databaseAuthentication;
        this.database = database;

        this.pluginDatabaseValues = pluginDatabaseValues;
        this.userDatabaseValues = userDatabaseValues;
    }

    @Override
    public void run() {
        this.databaseAuthentication.setHost("localhost");
        this.databaseAuthentication.setPort("3306");
        this.databaseAuthentication.setDatabase("discordbot");
        this.databaseAuthentication.setUsername("root");
        this.databaseAuthentication.setPassword("");

        this.database.connect();

        this.database.createTable(this.pluginDatabaseValues.getTableName(), this.pluginDatabaseValues.getAllColumnLabels());
        this.database.createTable(this.userDatabaseValues.getTableName(), this.userDatabaseValues.getAllColumnLabels());
    }

    @Override
    public void end() {

    }
}
