package net.aminecraftdev.discordbot.activator;

import net.aminecraftdev.discordbot.activator.interfaces.ISpigotActivator;
import net.aminecraftdev.spigot.site.SpigotSiteCore;


/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class SpigotActivator implements ISpigotActivator {

    @Override
    public void run() {
        System.out.println("Loading spigot information...");
        long currentMs = System.currentTimeMillis();
        long timeTaken;

        new SpigotSiteCore();

        timeTaken = (System.currentTimeMillis() - currentMs) / 1000;
        System.out.println("Spigot connection has been established. (Took " + timeTaken + "s)");
    }

    @Override
    public void end() {

    }
}
