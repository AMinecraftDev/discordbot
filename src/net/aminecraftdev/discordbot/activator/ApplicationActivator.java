package net.aminecraftdev.discordbot.activator;

import net.aminecraftdev.discordbot.activator.interfaces.IApplicationActivator;
import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.authentication.factory.IAuthenticationFactory;
import net.aminecraftdev.discordbot.event.interfaces.IDiscordEventProvider;
import net.aminecraftdev.discordbot.service.interfaces.IDiscordService;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class ApplicationActivator implements IApplicationActivator {

    private final IAuthenticationFactory _authenticationFactory;
    private final IDiscordEventProvider _discordEventProvider;
    private final IDiscordService _discordServer;
    private final IJdaProvider _jdaProvider;
    private final JDABuilder _builder;


    public ApplicationActivator(IDiscordService discordService, IAuthenticationFactory authenticationFactory, IJdaProvider jdaProvider, IDiscordEventProvider discordEventProvider) {
        _discordServer = discordService;
        _builder = new JDABuilder(AccountType.BOT);
        _authenticationFactory = authenticationFactory;
        _jdaProvider = jdaProvider;
        _discordEventProvider = discordEventProvider;
    }

    @Override
    public void run() {
        IAuthentication auth = _authenticationFactory.getInstance();

        auth.setPassword("NDM1MDY3NTUxNDcxNTAxMzEy.DbToMg.GDIoFeGVd-KMyF4EL-_qBDfxG1o");

        JDA jda = _discordServer.startBot(_builder, auth);

        _jdaProvider.setJDA(jda);
        _discordEventProvider.connectEventProvider();
    }

    @Override
    public void end() {

    }

}



