package net.aminecraftdev.discordbot.message;

import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IMessage;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageContext;
import net.dv8tion.jda.core.entities.ChannelType;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class MessageContext implements IMessageContext {

    private ICommandHandler commandHandler;
    private IMessage messageHandler;

    public MessageContext(ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public void setMessageHandler(IMessage messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public void digest(IDiscordMessage discordMessage) {
        IHandler<IDiscordMessage> completer = this.messageHandler.handle(discordMessage);

        if(completer == null) {
            System.out.println("> No one could properly execute the message. <");
        } else {
            System.out.println("> " + completer.getClass().getSimpleName() + " was able to execute the message. <");
            completer.getService().handleMessage(discordMessage);

            if(discordMessage.getMessage().getChannelType() == ChannelType.TEXT) {
                this.commandHandler.deleteMessage(discordMessage);
            }
        }
    }
}
