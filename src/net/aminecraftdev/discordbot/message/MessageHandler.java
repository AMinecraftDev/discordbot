package net.aminecraftdev.discordbot.message;

import net.aminecraftdev.discordbot.AMineriX;
import net.aminecraftdev.discordbot.message.interfaces.ICallback;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.exceptions.ErrorResponseException;
import net.dv8tion.jda.core.requests.RestAction;
import net.dv8tion.jda.core.requests.restaction.MessageAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public class MessageHandler implements IMessageHandler {

    private static char DATA[] = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
            'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9'
    };
    private static Random RAND = new Random();

    @Override
    public void handleEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder) {
        handleFinalEmbedMessage(messageChannel, embedBuilder);
    }

    @Override
    public void handleEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder, boolean selfDestruct, long delay) {
        handleFinalEmbedMessage(messageChannel, embedBuilder, msg -> {
            if(selfDestruct) {
                AMineriX.getTimer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        msg.delete().queue();
                    }
                }, delay*1000);
            }
        });
    }

    @Override
    public void handleMessage(RestAction<? extends MessageChannel> restAction, String message) {
        restAction.queue(channel -> handleMessage(channel, message));
    }

    @Override
    public void handleMessage(MessageChannel messageChannel, String message) {
        handleFinalMessage(messageChannel, message);
    }

    @Override
    public void handleMessage(MessageChannel messageChannel, String message, boolean selfDestruct, long delay) {
        handleFinalMessage(messageChannel, message, msg -> {
            if(selfDestruct) {
                AMineriX.getTimer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        msg.delete().queue();
                    }
                }, delay*1000);
            }
        });
    }

    @Override
    public MessageChannel getMessageChannel(IDiscordMessage discordMessage) {
        return discordMessage.getMessage().getChannel();
    }

    @Override
    public String getGuildId(IDiscordMessage discordMessage) {
        return discordMessage.getMessage().getTextChannel().getGuild().getId();
    }

    @Override
    public String generateNewCaptcha() {
        char index[] = new char[10];

        for(int i = 0; i < index.length; i++) {
            int random = RAND.nextInt(DATA.length);

            index[i] = DATA[random];
        }

        return new String(index);
    }

    private void handleFinalMessage(MessageChannel messageChannel, String message) {
        handleFinalMessage(messageChannel, message, call->{});
    }

    private void handleFinalEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder) {
        handleFinalEmbedMessage(messageChannel, embedBuilder, call->{});
    }

    private void handleFinalMessage(MessageChannel messageChannel, String message, ICallback<Message> callback) {
        if(message.length() >= 2000) {
            List<String> strings = new ArrayList<>();
            int index = 0;

            while(index < message.length()) {
                int value = message.lastIndexOf("\n\n", index+2000);

                if(value == -1) break;

                strings.add(message.substring(index, value));
                index += value;
            }

            strings.forEach(msg -> messageChannel.sendMessage(msg).queue(callback::call));
            return;
        }

        messageChannel.sendMessage(message).queue(callback::call);
    }

    private void handleFinalEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder, ICallback<Message> callback) {
        messageChannel.sendMessage(embedBuilder.build()).queue(callback::call);
    }
}
