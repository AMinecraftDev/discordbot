package net.aminecraftdev.discordbot.message;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Message;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class DiscordMessage implements IDiscordMessage {

    private Message message;
    private IDiscordUser discordUser;

    @Override
    public Message getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public IDiscordUser getDiscordUser() {
        return this.discordUser;
    }

    @Override
    public void setDiscordUser(IDiscordUser member) {
        this.discordUser = member;
    }
}
