package net.aminecraftdev.discordbot.message;

import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.dv8tion.jda.core.entities.Message;

import java.util.function.Consumer;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public class CommandHandler implements ICommandHandler {

    @Override
    public boolean canHandle(IDiscordMessage discordMessage, IContains contains) {
        boolean containsString = false;

        for(String s : contains.getContains()) {
            if(discordMessage.getMessage().getContentRaw().contains(s)) {
                containsString = true;
                break;
            }
        }

        return containsString;
    }

    @Override
    public boolean canHandle(IDiscordMessage discordMessage, IPrefix prefix) {
        return (discordMessage.getMessage().getContentRaw().startsWith(prefix.getPrefix()));
    }

    @Override
    public IHandler<IDiscordMessage> handleCommand(IDiscordMessage discordMessage, IMessage message) {
        if(message.canHandle(discordMessage)) {
            return message;
        } else {
            if(message.getSuccessor() != null) {
                return message.getSuccessor().handle(discordMessage);
            }
        }

        return null;
    }

    @Override
    public String getTrimmedCommand(IDiscordMessage discordMessage, IPrefix prefix) {
        Message message = discordMessage.getMessage();

        return message.getContentRaw().replace(prefix.getPrefix(), "").trim();
    }

    @Override
    public void deleteMessage(IDiscordMessage discordMessage) {
        deleteMessage(discordMessage.getMessage());
    }

    @Override
    public void deleteMessage(Message message) {
        deleteMessage(message, null);
    }

    @Override
    public void deleteMessage(Message message, Consumer<Void> consumer) {
        if(consumer != null) {
            message.delete().queue(consumer);
        } else {
            message.delete().queue();
        }
    }

}
