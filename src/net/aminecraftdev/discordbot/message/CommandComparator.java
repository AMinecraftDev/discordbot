package net.aminecraftdev.discordbot.message;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.message.interfaces.ICommandComparator;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class CommandComparator implements ICommandComparator {

    @Override
    public int compare(ICommand o1, ICommand o2) {
        return o1.getClass().getName().compareToIgnoreCase(o2.getClass().getName());
    }
}
