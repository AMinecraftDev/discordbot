package net.aminecraftdev.discordbot.message.interfaces;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Message;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IDiscordMessage {

    Message getMessage();

    void setMessage(Message message);

    IDiscordUser getDiscordUser();

    void setDiscordUser(IDiscordUser discordUser);

}
