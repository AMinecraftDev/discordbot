package net.aminecraftdev.discordbot.message.interfaces;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.requests.RestAction;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public interface IMessageHandler {

    void handleEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder);

    void handleEmbedMessage(MessageChannel messageChannel, EmbedBuilder embedBuilder, boolean selfDestruct, long delay);

    void handleMessage(RestAction<? extends MessageChannel> restAction, String message);

    void handleMessage(MessageChannel messageChannel, String message);

    void handleMessage(MessageChannel messageChannel, String message, boolean selfDestruct, long delay);

    MessageChannel getMessageChannel(IDiscordMessage discordMessage);

    String getGuildId(IDiscordMessage discordMessage);

    String generateNewCaptcha();
}
