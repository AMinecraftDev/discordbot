package net.aminecraftdev.discordbot.message.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IMessageContext {

    void setMessageHandler(IMessage messageHandler);

    void digest(IDiscordMessage discordMessage);

}
