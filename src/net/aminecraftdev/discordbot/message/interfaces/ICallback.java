package net.aminecraftdev.discordbot.message.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public interface ICallback<T> {

    void call(T t);

}
