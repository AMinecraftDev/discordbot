package net.aminecraftdev.discordbot.message.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;

import java.util.Comparator;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public interface ICommandComparator extends Comparator<ICommand> {
}
