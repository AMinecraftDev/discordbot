package net.aminecraftdev.discordbot.message.factory;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public class DiscordMessageFactory implements IDiscordMessageFactory {

    private IDiscordMessage discordMessage;

    public DiscordMessageFactory(IDiscordMessage discordMessage) {
        this.discordMessage = discordMessage;
    }

    @Override
    public IDiscordMessage getInstance() {
        try {
            return this.discordMessage.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
