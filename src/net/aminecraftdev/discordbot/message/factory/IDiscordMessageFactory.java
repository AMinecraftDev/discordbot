package net.aminecraftdev.discordbot.message.factory;

import net.aminecraftdev.discordbot.factory.IFactory;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public interface IDiscordMessageFactory extends IFactory<IDiscordMessage> {
}
