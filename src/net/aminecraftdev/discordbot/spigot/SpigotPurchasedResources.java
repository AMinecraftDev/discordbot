package net.aminecraftdev.discordbot.spigot;

import net.aminecraftdev.discordbot.container.interfaces.ISpigotUserContainer;
import net.aminecraftdev.discordbot.spigot.interfaces.ISpigotPurchasedResources;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.resource.PremiumResource;
import net.aminecraftdev.spigot.api.resource.Resource;
import net.aminecraftdev.spigot.api.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 10-Jun-18
 */
public class SpigotPurchasedResources implements ISpigotPurchasedResources {

    private ISpigotUserContainer spigotUserContainer;

    public SpigotPurchasedResources(ISpigotUserContainer spigotUserContainer) {
        this.spigotUserContainer = spigotUserContainer;
    }

    @Override
    public List<Resource> getPurchasedResources(IDiscordUser discordUser)
                throws ConnectionFailedException {
        List<Resource> purchased = new ArrayList<>();

        for(User user : this.spigotUserContainer.getLoggedInUsers()) {
            List<Resource> created = user.getCreatedResources();

            for(Resource resource : created) {
                if(resource instanceof PremiumResource) {
                    PremiumResource premiumResource = (PremiumResource) resource;

                    SpigotSite.getAPI().getResourceManager().getPremiumResourceBuyers(premiumResource, user);

                    if(premiumResource.isBuyer(discordUser.getSpigotMember())) {
                        purchased.add(premiumResource);
                    }
                }
            }
        }

        return purchased;
    }

}
