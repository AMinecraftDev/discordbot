package net.aminecraftdev.discordbot.spigot.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 07-Jul-19
 */
public interface ISpigotResourceUpdate {

    String getSpigotResourceUpdateLink(int pluginId, String pluginName);
}
