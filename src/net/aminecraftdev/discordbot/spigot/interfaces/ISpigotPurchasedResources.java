package net.aminecraftdev.discordbot.spigot.interfaces;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.resource.Resource;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 10-Jun-18
 */
public interface ISpigotPurchasedResources {
    List<Resource> getPurchasedResources(IDiscordUser discordUser)
                throws ConnectionFailedException;
}
