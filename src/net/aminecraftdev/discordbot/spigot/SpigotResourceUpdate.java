package net.aminecraftdev.discordbot.spigot;

import net.aminecraftdev.discordbot.spigot.interfaces.ISpigotResourceUpdate;
import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.resource.Resource;
import net.aminecraftdev.spigot.api.resource.ResourceUpdate;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 07-Jul-19
 */
public class SpigotResourceUpdate implements ISpigotResourceUpdate {

    @Override
    public String getSpigotResourceUpdateLink(int pluginId, String pluginName) {
        try {
            Resource resource = SpigotSite.getAPI().getResourceManager().getResourceById(pluginId);

            if(resource == null) return null;
            if(!resource.getResourceName().contains(pluginName)) return null;

            List<ResourceUpdate> resourceUpdates = resource.getResourceUpdates();

            if(resourceUpdates.isEmpty()) return null;

            ResourceUpdate latest = resourceUpdates.get(0);

            if(latest == null) return null;

            return latest.getUpdateLink();
        } catch (ConnectionFailedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
