package net.aminecraftdev.discordbot.file;


import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsDataFactory;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsPathFactory;
import net.aminecraftdev.discordbot.file.handler.IFileHandler;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class SettingsFile implements ISettingsFile {

    private final ISettingsPathFactory settingsPathFactory;
    private final ISettingsDataFactory settingsDataFactory;
    private final IFileHandler fileHandler;

    private Map<String, Object> data;

    public SettingsFile(ISettingsDataFactory settingsDataFactory, IFileHandler fileHandler, ISettingsPathFactory settingsPathFactory) {
        this.settingsDataFactory = settingsDataFactory;
        this.settingsPathFactory = settingsPathFactory;
        this.fileHandler = fileHandler;
    }

    @Override
    public void loadData() {
        this.data = this.settingsDataFactory.getInstance();

        if(this.data == null) this.data = new HashMap<>();
    }

    @Override
    public void saveData() {
        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), this.data);
    }

    @Override
    public Map<String, Object> getMapOfData() {
        return this.data;
    }

    @Override
    public Object getValue(String key) {
        return this.data.getOrDefault(key, -1);
    }

    @Override
    public void removeKey(String key) {
        this.data.remove(key);
    }

    @Override
    public void updateValue(String key, Object value) {
        this.data.put(key, value);
    }
}
