package net.aminecraftdev.discordbot.file.factory;


import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsDataFactory;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsPathFactory;
import net.aminecraftdev.discordbot.file.handler.IFileHandler;

import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class SettingsDataFactory implements ISettingsDataFactory {

    private ISettingsPathFactory settingsPathFactory;
    private IFileHandler fileHandler;

    public SettingsDataFactory(IFileHandler fileHandler, ISettingsPathFactory settingsPathFactory) {
        this.settingsPathFactory = settingsPathFactory;
        this.fileHandler = fileHandler;
    }

    @Override
    public Map<String, Object> getInstance() {
        return this.fileHandler.loadFile(this.settingsPathFactory.getInstance());
    }
}
