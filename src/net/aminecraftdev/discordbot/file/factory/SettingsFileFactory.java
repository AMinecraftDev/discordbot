package net.aminecraftdev.discordbot.file.factory;

import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsDataFactory;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsFileFactory;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsPathFactory;
import net.aminecraftdev.discordbot.file.handler.IFileHandler;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class SettingsFileFactory implements ISettingsFileFactory {

    private ISettingsPathFactory settingsPathFactory;
    private ISettingsDataFactory settingsDataFactory;
    private ISettingsFile settingsFile;
    private IFileHandler fileHandler;

    public SettingsFileFactory(ISettingsFile settingsFile, ISettingsDataFactory settingsDataFactory, IFileHandler fileHandler, ISettingsPathFactory settingsPathFactory) {
        this.settingsFile = settingsFile;
        this.fileHandler = fileHandler;
        this.settingsDataFactory = settingsDataFactory;
        this.settingsPathFactory = settingsPathFactory;
    }

    @Override
    public ISettingsFile getInstance() {
        try {
            Class[] variables = {ISettingsDataFactory.class, IFileHandler.class, ISettingsPathFactory.class};
            ISettingsFile settingsFile = this.settingsFile.getClass().getConstructor(variables).newInstance(this.settingsDataFactory, this.fileHandler, this.settingsPathFactory);

            settingsFile.loadData();

            return settingsFile;
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
