package net.aminecraftdev.discordbot.file.factory;

import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsPathFactory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class SettingsPathFactory implements ISettingsPathFactory {

    @Override
    public String getInstance() {
        return "D:\\Minecraft Java Projects\\Discord Bots\\DiscordBot\\settings.yml";
    }
}
