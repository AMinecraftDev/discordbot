package net.aminecraftdev.discordbot.file.factory.interfaces;


import net.aminecraftdev.discordbot.factory.IFactory;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ISettingsFileFactory extends IFactory<ISettingsFile> {

}
