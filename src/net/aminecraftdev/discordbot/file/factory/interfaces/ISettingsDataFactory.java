package net.aminecraftdev.discordbot.file.factory.interfaces;


import net.aminecraftdev.discordbot.factory.IFactory;

import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ISettingsDataFactory extends IFactory<Map<String, Object>> {
}
