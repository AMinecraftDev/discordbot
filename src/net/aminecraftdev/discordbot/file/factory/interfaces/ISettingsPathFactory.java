package net.aminecraftdev.discordbot.file.factory.interfaces;


import net.aminecraftdev.discordbot.factory.IFactory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ISettingsPathFactory extends IFactory<String> {
}
