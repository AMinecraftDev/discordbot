package net.aminecraftdev.discordbot.file.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ISettingsFile extends IFile {
}
