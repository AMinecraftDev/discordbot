package net.aminecraftdev.discordbot.file.interfaces;

import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IFile {

    void loadData();

    void saveData();

    Map<String, Object> getMapOfData();

    Object getValue(String key);

    void removeKey(String key);

    void updateValue(String key, Object value);

}
