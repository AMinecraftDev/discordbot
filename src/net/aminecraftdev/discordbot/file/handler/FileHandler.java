package net.aminecraftdev.discordbot.file.handler;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class FileHandler implements IFileHandler {

    private Yaml yaml = new Yaml();

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> loadFile(String path) {
        Map<String, Object> map = null;

        try {
            map = (HashMap<String, Object>) this.yaml.load(new FileReader(new File(path)));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        return map;
    }

    @Override
    public void saveFile(String path, Map<String, Object> file) {
        try {
            File yamlFile = new File(path);

            try {
                this.yaml.dump(file, new FileWriter(path));
            } catch (FileNotFoundException ex) {
                throw new Exception("Could not find file " + yamlFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
