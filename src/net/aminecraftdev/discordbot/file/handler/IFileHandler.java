package net.aminecraftdev.discordbot.file.handler;

import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IFileHandler {

    Map<String, Object> loadFile(String path);

    void saveFile(String path, Map<String, Object> file);

}
