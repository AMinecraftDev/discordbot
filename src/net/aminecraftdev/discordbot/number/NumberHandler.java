package net.aminecraftdev.discordbot.number;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 19-Jun-18
 */
public class NumberHandler implements INumberHandler {

    @Override
    public boolean isDouble(String input) {
        try {
            Double.valueOf(input);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean isInt(String input) {
        try {
            Integer.valueOf(input);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public Integer getInt(String input) {
        if(!isInt(input)) return null;

        return Integer.valueOf(input);
    }

    @Override
    public Double getDouble(String input) {
        if(!isDouble(input)) return null;

        return Double.valueOf(input);
    }

}
