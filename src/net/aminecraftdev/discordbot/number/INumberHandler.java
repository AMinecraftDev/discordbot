package net.aminecraftdev.discordbot.number;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 19-Jun-18
 */
public interface INumberHandler {

    boolean isDouble(String input);

    boolean isInt(String input);

    Integer getInt(String input);

    Double getDouble(String input);
}
