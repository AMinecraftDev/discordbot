package net.aminecraftdev.discordbot.comparator;

import net.aminecraftdev.discordbot.comparator.interfaces.IChannelComparator;
import net.dv8tion.jda.core.entities.Channel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-May-19
 */
public class ChannelComparator implements IChannelComparator {

    @Override
    public int compare(Channel o1, Channel o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}
