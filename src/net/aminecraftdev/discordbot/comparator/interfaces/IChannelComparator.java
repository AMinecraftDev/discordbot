package net.aminecraftdev.discordbot.comparator.interfaces;

import net.dv8tion.jda.core.entities.Channel;

import java.util.Comparator;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-May-19
 */
public interface IChannelComparator extends Comparator<Channel> {
}
