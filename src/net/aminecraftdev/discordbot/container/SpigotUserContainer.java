package net.aminecraftdev.discordbot.container;

import net.aminecraftdev.discordbot.container.interfaces.ISpigotUserContainer;
import net.aminecraftdev.spigot.api.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class SpigotUserContainer implements ISpigotUserContainer {

    private List<User> spigotUsers = new ArrayList<>();

    @Override
    public List<User> getLoggedInUsers() {
        return new ArrayList<>(this.spigotUsers);
    }

    @Override
    public User getRingLeader() {
        return getLoggedInUsers().stream().filter(user -> user.getUsername().equalsIgnoreCase("AMinecraftDev")).findFirst().orElse(null);
    }

    @Override
    public void addUser(User user) {
        this.spigotUsers.add(user);
    }
}
