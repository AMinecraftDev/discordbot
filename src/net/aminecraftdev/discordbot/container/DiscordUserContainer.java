package net.aminecraftdev.discordbot.container;

import net.aminecraftdev.discordbot.container.interfaces.IDiscordUserContainer;
import net.aminecraftdev.discordbot.user.factory.IDiscordUserFactory;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public class DiscordUserContainer implements IDiscordUserContainer {

    private List<IDiscordUser> discordUsers = new ArrayList<>();
    private IDiscordUserFactory discordUserFactory;

    public DiscordUserContainer(IDiscordUserFactory discordUserFactory) {
        this.discordUserFactory = discordUserFactory;
    }

    @Override
    public IDiscordUser getUser(Member member) {
        Optional<IDiscordUser> optional = new ArrayList<>(this.discordUsers).stream().filter(user -> user.getDiscordId() == member.getUser().getIdLong()).findFirst();

        if(optional.isPresent()) {
            IDiscordUser discordUser = optional.get();

            if(discordUser.getDiscordMember() == null) {
                discordUser.setDiscordMember(member);
            }

            return discordUser;
        }

        IDiscordUser discordUser = this.discordUserFactory.getInstance();

        discordUser.setDiscordMember(member);
        discordUser.loadFromDatabase();

        this.discordUsers.add(discordUser);

        return discordUser;
    }

    @Override
    public IDiscordUser getUser(User user) {
        Optional<IDiscordUser> optional = new ArrayList<>(this.discordUsers).stream().filter(user1 -> user1.getDiscordId() == user.getIdLong()).findFirst();

        if(optional.isPresent()) return optional.get();

        IDiscordUser discordUser = this.discordUserFactory.getInstance();

        discordUser.setDiscordUser(user);
        discordUser.loadFromDatabase();
        this.discordUsers.add(discordUser);

        return discordUser;
    }
}
