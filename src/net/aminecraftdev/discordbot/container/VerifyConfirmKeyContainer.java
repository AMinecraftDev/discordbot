package net.aminecraftdev.discordbot.container;

import net.aminecraftdev.discordbot.container.interfaces.IVerifyConfirmKeyContainer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public class VerifyConfirmKeyContainer implements IVerifyConfirmKeyContainer {

    private Map<String, Integer> confirmKeyMap = new HashMap<>();

    @Override
    public void saveKey(String confirmationKey, int spigotId) {
        this.confirmKeyMap.put(confirmationKey, spigotId);
    }

    @Override
    public boolean isInContainer(String confirmationKey) {
        return this.confirmKeyMap.containsKey(confirmationKey);
    }

    @Override
    public int getSpigotId(String confirmationKey) {
        return this.confirmKeyMap.get(confirmationKey);
    }

    @Override
    public void removeConfirmationKey(String confirmationKey) {
        this.confirmKeyMap.remove(confirmationKey);
    }

}
