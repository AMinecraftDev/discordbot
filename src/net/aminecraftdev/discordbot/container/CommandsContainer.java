package net.aminecraftdev.discordbot.container;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.container.interfaces.ICommandsContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public class CommandsContainer implements ICommandsContainer {

    private List<ICommand> commands = new ArrayList<>();

    @Override
    public List<ICommand> getCommands() {
        return this.commands;
    }
}
