package net.aminecraftdev.discordbot.container.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public interface IVerifyConfirmKeyContainer {

    void saveKey(String confirmationKey, int spigotId);

    boolean isInContainer(String confirmationKey);

    int getSpigotId(String confirmationKey);

    void removeConfirmationKey(String confirmationKey);
}
