package net.aminecraftdev.discordbot.container.interfaces;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.User;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public interface IDiscordUserContainer {

    IDiscordUser getUser(Member member);

    IDiscordUser getUser(User user);

}
