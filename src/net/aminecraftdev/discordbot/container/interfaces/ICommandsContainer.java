package net.aminecraftdev.discordbot.container.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public interface ICommandsContainer {

    List<ICommand> getCommands();

}
