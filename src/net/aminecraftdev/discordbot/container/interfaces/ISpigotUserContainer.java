package net.aminecraftdev.discordbot.container.interfaces;

import net.aminecraftdev.spigot.api.user.User;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface ISpigotUserContainer {

    List<User> getLoggedInUsers();

    User getRingLeader();

    void addUser(User user);

}
