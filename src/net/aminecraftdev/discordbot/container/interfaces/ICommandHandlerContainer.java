package net.aminecraftdev.discordbot.container.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface ICommandHandlerContainer {

    List<IMessage> getCommands();

}
