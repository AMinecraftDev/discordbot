package net.aminecraftdev.discordbot.container;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;
import net.aminecraftdev.discordbot.container.interfaces.ICommandHandlerContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class CommandHandlerContainer implements ICommandHandlerContainer {

    private List<IMessage> commands = new ArrayList<>();

    @Override
    public List<IMessage> getCommands() {
        return this.commands;
    }
}
