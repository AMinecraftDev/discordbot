package net.aminecraftdev.discordbot.category;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryPermissionHandler;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.requests.restaction.PermissionOverrideAction;

import java.util.Arrays;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Jun-19
 */
public class CategoryPermissionHandler implements ICategoryPermissionHandler {

    @Override
    public void applyAccessToViewChannel(Channel channel, List<Role> rolesWithViewAccess) {
        List<Permission> allowCategoryPermissions = Arrays.asList(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ, Permission.MESSAGE_HISTORY);
        List<Permission> denyCategoryPermissions = Arrays.asList(Permission.MESSAGE_WRITE, Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES, Permission.CREATE_INSTANT_INVITE, Permission.KICK_MEMBERS, Permission.BAN_MEMBERS, Permission.ADMINISTRATOR, Permission.MANAGE_CHANNEL, Permission.MANAGE_SERVER, Permission.MESSAGE_ADD_REACTION, Permission.VIEW_AUDIT_LOGS, Permission.PRIORITY_SPEAKER, Permission.MESSAGE_TTS, Permission.MESSAGE_MANAGE, Permission.MESSAGE_MENTION_EVERYONE, Permission.MESSAGE_EXT_EMOJI, Permission.VOICE_CONNECT, Permission.VOICE_SPEAK, Permission.VOICE_MUTE_OTHERS, Permission.VOICE_DEAF_OTHERS, Permission.VOICE_MOVE_OTHERS, Permission.VOICE_USE_VAD, Permission.NICKNAME_CHANGE, Permission.NICKNAME_MANAGE, Permission.MANAGE_ROLES, Permission.MANAGE_PERMISSIONS, Permission.MANAGE_WEBHOOKS, Permission.MANAGE_EMOTES);

        rolesWithViewAccess.forEach(role -> {
            PermissionOverrideAction permissionOverrideAction = channel.createPermissionOverride(role);

            permissionOverrideAction.setAllow(allowCategoryPermissions).queue();
            permissionOverrideAction.setDeny(denyCategoryPermissions).queue();
        });
    }

    @Override
    public void applyBasicAccessToChannel(Channel channel, List<Role> rolesWithBasicAccess) {
        List<Permission> allowPermissions = Arrays.asList(Permission.VIEW_CHANNEL, Permission.MESSAGE_READ, Permission.MESSAGE_WRITE, Permission.MESSAGE_HISTORY, Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES);
        List<Permission> denyPermissions = Arrays.asList(Permission.CREATE_INSTANT_INVITE, Permission.KICK_MEMBERS, Permission.BAN_MEMBERS, Permission.ADMINISTRATOR, Permission.MANAGE_CHANNEL, Permission.MANAGE_SERVER, Permission.MESSAGE_ADD_REACTION, Permission.VIEW_AUDIT_LOGS, Permission.PRIORITY_SPEAKER, Permission.MESSAGE_TTS, Permission.MESSAGE_MANAGE, Permission.MESSAGE_MENTION_EVERYONE, Permission.MESSAGE_EXT_EMOJI, Permission.VOICE_CONNECT, Permission.VOICE_SPEAK, Permission.VOICE_MUTE_OTHERS, Permission.VOICE_DEAF_OTHERS, Permission.VOICE_MOVE_OTHERS, Permission.VOICE_USE_VAD, Permission.NICKNAME_CHANGE, Permission.NICKNAME_MANAGE, Permission.MANAGE_ROLES, Permission.MANAGE_PERMISSIONS, Permission.MANAGE_WEBHOOKS, Permission.MANAGE_EMOTES);

        rolesWithBasicAccess.forEach(role -> {
            PermissionOverrideAction permissionOverrideAction = channel.createPermissionOverride(role);

            permissionOverrideAction.setAllow(allowPermissions).queue();
            permissionOverrideAction.setDeny(denyPermissions).queue();
        });
    }

    @Override
    public void removeAccessToChannel(Channel channel, Role role) {
        List<Permission> permissions = Arrays.asList(Permission.CREATE_INSTANT_INVITE, Permission.VIEW_CHANNEL, Permission.MESSAGE_READ,
                Permission.MESSAGE_WRITE, Permission.MESSAGE_TTS, Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES,
                Permission.MESSAGE_HISTORY, Permission.MESSAGE_MENTION_EVERYONE, Permission.MESSAGE_EXT_EMOJI);

        channel.putPermissionOverride(role).setDeny(permissions).queue();
    }

}
