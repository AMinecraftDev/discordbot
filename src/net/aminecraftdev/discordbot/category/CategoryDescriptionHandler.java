package net.aminecraftdev.discordbot.category;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryDescriptionHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 11-Jun-19
 */
public class CategoryDescriptionHandler implements ICategoryDescriptionHandler {

    private IJdaProvider jdaProvider;

    public CategoryDescriptionHandler(IJdaProvider jdaProvider) {
        this.jdaProvider = jdaProvider;
    }

    @Override
    public void updateChannelDescriptions(String name, boolean ignoreCase, String topic) {
        Guild guild = this.jdaProvider.getDefaultGuild();
        List<Category> categories = guild.getCategoriesByName(name, ignoreCase);

        categories.forEach(category -> category.getTextChannels().forEach(textChannel ->
            textChannel.getManager().setTopic(topic).queue()));
    }

}
