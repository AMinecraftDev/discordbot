package net.aminecraftdev.discordbot.category;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryNameHandler;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 11-Jun-19
 */
public class CategoryNameHandler implements ICategoryNameHandler {

    @Override
    public String getCategoryName(String pluginName) {
        return "━» " + pluginName + " «━";
    }

}
