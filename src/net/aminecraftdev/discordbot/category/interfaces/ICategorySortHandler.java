package net.aminecraftdev.discordbot.category.interfaces;

import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Jun-19
 */
public interface ICategorySortHandler {

    void sortCustomPluginCategories(Guild guild, GuildController guildController);

    void sortInnerCategoryChannels(Category category, GuildController guildController);
}
