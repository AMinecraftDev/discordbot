package net.aminecraftdev.discordbot.category.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 11-Jun-19
 */
public interface ICategoryDescriptionHandler {

    void updateChannelDescriptions(String name, boolean ignoreCase, String topic);
}
