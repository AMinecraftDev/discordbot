package net.aminecraftdev.discordbot.category.interfaces;

import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Role;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Jun-19
 */
public interface ICategoryPermissionHandler {

    void applyAccessToViewChannel(Channel channel, List<Role> rolesWithViewAccess);

    void applyBasicAccessToChannel(Channel channel, List<Role> rolesWithBasicAccess);

    void removeAccessToChannel(Channel channel, Role role);
}
