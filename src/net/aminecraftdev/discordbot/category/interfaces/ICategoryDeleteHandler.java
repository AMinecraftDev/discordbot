package net.aminecraftdev.discordbot.category.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Jun-19
 */
public interface ICategoryDeleteHandler {

    void deleteCategory(String name, boolean ignoreCase, boolean deleteChannels);
}
