package net.aminecraftdev.discordbot.category;

import net.aminecraftdev.discordbot.category.interfaces.ICategorySortHandler;
import net.aminecraftdev.discordbot.comparator.interfaces.IChannelComparator;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.requests.RestAction;
import net.dv8tion.jda.core.requests.restaction.order.ChannelOrderAction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Jun-19
 */
public class CategorySortHandler implements ICategorySortHandler {

    private IChannelComparator channelComparator;

    public CategorySortHandler(IChannelComparator channelComparator) {
        this.channelComparator = channelComparator;
    }

    @Override
    public void sortCustomPluginCategories(Guild guild, GuildController guildController) {
        List<Category> categoriesToBeSorted = new ArrayList<>();

        guild.getCategories().stream().filter(ctg -> ctg.getName().startsWith("━»")).forEach(categoriesToBeSorted::add);

        categoriesToBeSorted.sort(this.channelComparator);

        int position = 2;

        List<ChannelOrderAction<Category>> actions = new ArrayList<>();
        ChannelOrderAction<Category> modifyCategories = guildController.modifyCategoryPositions();

        for (Category ctg : categoriesToBeSorted) {
            actions.add(modifyCategories.selectPosition(ctg).moveTo(position));
            position++;
        }

        guildController.modifyCategoryPositions().queue(nonExistent -> {
            int delay = 1;

            for (RestAction restAction : actions) {
                restAction.queueAfter(delay, TimeUnit.SECONDS);
                delay++;
            }
        });
    }

    @Override
    public void sortInnerCategoryChannels(Category category, GuildController guildController) {
        List<TextChannel> channelsToBeSorted = new ArrayList<>(category.getTextChannels());

        channelsToBeSorted.sort(this.channelComparator);

        int position = 0;

        List<ChannelOrderAction<TextChannel>> actions = new ArrayList<>();
        ChannelOrderAction<TextChannel> modifyChannelPositions = category.modifyTextChannelPositions();

        for(TextChannel textChannel : channelsToBeSorted) {
            actions.add(modifyChannelPositions.selectPosition(textChannel).moveTo(position));
            position++;
        }

        guildController.modifyCategoryPositions().queue(nonExistent -> {
            int delay = 1;

            for(RestAction restAction : actions) {
                restAction.queueAfter(delay, TimeUnit.SECONDS);
                delay++;
            }
        });
    }

}
