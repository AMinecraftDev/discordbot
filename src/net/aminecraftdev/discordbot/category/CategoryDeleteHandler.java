package net.aminecraftdev.discordbot.category;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryDeleteHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Jun-19
 */
public class CategoryDeleteHandler implements ICategoryDeleteHandler {

    private IJdaProvider jdaProvider;

    public CategoryDeleteHandler(IJdaProvider jdaProvider) {
        this.jdaProvider = jdaProvider;
    }

    @Override
    public void deleteCategory(String name, boolean ignoreCase, boolean deleteChannels) {
        Guild guild = this.jdaProvider.getDefaultGuild();
        List<Category> categories = guild.getCategoriesByName(name, ignoreCase);

        if(deleteChannels) {
            categories.forEach(category -> category.getChannels().forEach(channel -> channel.delete().queue()));
        }

        categories.forEach(category -> category.delete().queue());
    }

}
