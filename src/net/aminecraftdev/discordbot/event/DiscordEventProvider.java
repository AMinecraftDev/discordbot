package net.aminecraftdev.discordbot.event;

import net.aminecraftdev.discordbot.container.interfaces.IDiscordUserContainer;
import net.aminecraftdev.discordbot.event.interfaces.IDiscordEventProvider;
import net.aminecraftdev.discordbot.message.factory.IDiscordMessageFactory;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageContext;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */

public class DiscordEventProvider extends ListenerAdapter implements IDiscordEventProvider {

    private final IDiscordUserContainer discordUserContainer;
    private final IMessageContext messageContext;
    private final IJdaProvider jdaProvider;
    private final IDiscordMessageFactory discordMessageFactory;

    public DiscordEventProvider(IJdaProvider jdaProvider, IMessageContext messageContext, IDiscordMessageFactory discordMessageFactory, IDiscordUserContainer discordUserContainer) {
        this.jdaProvider = jdaProvider;
        this.messageContext = messageContext;
        this.discordMessageFactory = discordMessageFactory;
        this.discordUserContainer = discordUserContainer;
    }

    @Override
    public void connectEventProvider() {
        this.jdaProvider.getJDA().addEventListener(this);
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();

        if(event.getAuthor().isBot() || event.getAuthor().isFake()) return;

        IDiscordMessage discordMessage = this.discordMessageFactory.getInstance();

        if(discordMessage == null) return;

        IDiscordUser discordUser;

        if(event.getMember() != null) {
            discordUser = this.discordUserContainer.getUser(event.getMember());
        } else {
            discordUser = this.discordUserContainer.getUser(event.getAuthor());
        }

        discordMessage.setMessage(message);
        discordMessage.setDiscordUser(discordUser);

        this.messageContext.digest(discordMessage);

        if(message.getChannel().getName().equalsIgnoreCase("verify") || message.getContentRaw().startsWith("!")) {
            message.delete().queue();
        }
    }
}
