package net.aminecraftdev.discordbot.event.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public interface IEventProvider {

    void connectEventProvider();
}
