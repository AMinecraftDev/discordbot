package net.aminecraftdev.discordbot.event.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IDiscordEventProvider extends IEventProvider {

}
