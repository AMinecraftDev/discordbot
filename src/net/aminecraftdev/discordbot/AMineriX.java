package net.aminecraftdev.discordbot;

import net.aminecraftdev.discordbot.user.Role;
import java.util.Timer;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 15-Apr-18
 *
 * TODO: Add mute system
 */
public class AMineriX {

    private static Timer TIMER = new Timer();

    public static void main(String[] args) {
        Role.SetupRoles();
        new CompositeRoot();
    }

    public static Timer getTimer() {
        return TIMER;
    }

}
