package net.aminecraftdev.discordbot;

import eu.lestard.easydi.EasyDI;
import net.aminecraftdev.discordbot.activator.interfaces.*;
import net.aminecraftdev.discordbot.activator.*;
import net.aminecraftdev.discordbot.authentication.factory.*;
import net.aminecraftdev.discordbot.authentication.*;
import net.aminecraftdev.discordbot.builder.category.*;
import net.aminecraftdev.discordbot.builder.category.factory.*;
import net.aminecraftdev.discordbot.builder.interfaces.*;
import net.aminecraftdev.discordbot.category.*;
import net.aminecraftdev.discordbot.category.interfaces.*;
import net.aminecraftdev.discordbot.commands.alert.*;
import net.aminecraftdev.discordbot.commands.alert.interfaces.*;
import net.aminecraftdev.discordbot.commands.confirm.*;
import net.aminecraftdev.discordbot.commands.confirm.interfaces.*;
import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.commands.*;
import net.aminecraftdev.discordbot.commands.help.interfaces.*;
import net.aminecraftdev.discordbot.commands.help.*;
import net.aminecraftdev.discordbot.commands.login.*;
import net.aminecraftdev.discordbot.commands.login.interfaces.*;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.*;
import net.aminecraftdev.discordbot.commands.lookup.*;
import net.aminecraftdev.discordbot.commands.plugin.*;
import net.aminecraftdev.discordbot.commands.plugin.arguments.*;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.*;
import net.aminecraftdev.discordbot.commands.setup.factory.*;
import net.aminecraftdev.discordbot.commands.setup.interfaces.*;
import net.aminecraftdev.discordbot.commands.setup.*;
import net.aminecraftdev.discordbot.commands.verify.interfaces.*;
import net.aminecraftdev.discordbot.commands.verify.*;
import net.aminecraftdev.discordbot.comparator.*;
import net.aminecraftdev.discordbot.comparator.interfaces.*;
import net.aminecraftdev.discordbot.container.interfaces.*;
import net.aminecraftdev.discordbot.container.*;
import net.aminecraftdev.discordbot.database.authentication.*;
import net.aminecraftdev.discordbot.database.interfaces.*;
import net.aminecraftdev.discordbot.database.*;
import net.aminecraftdev.discordbot.database.plugins.*;
import net.aminecraftdev.discordbot.database.users.*;
import net.aminecraftdev.discordbot.event.interfaces.*;
import net.aminecraftdev.discordbot.event.*;
import net.aminecraftdev.discordbot.file.*;
import net.aminecraftdev.discordbot.file.factory.*;
import net.aminecraftdev.discordbot.file.factory.interfaces.*;
import net.aminecraftdev.discordbot.file.handler.*;
import net.aminecraftdev.discordbot.file.interfaces.*;
import net.aminecraftdev.discordbot.listeners.badwords.*;
import net.aminecraftdev.discordbot.listeners.badwords.interfaces.*;
import net.aminecraftdev.discordbot.listeners.linksharing.*;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.*;
import net.aminecraftdev.discordbot.logger.alerts.*;
import net.aminecraftdev.discordbot.logger.botlogs.*;
import net.aminecraftdev.discordbot.logger.giveaway.*;
import net.aminecraftdev.discordbot.logger.scoreboard.*;
import net.aminecraftdev.discordbot.message.factory.*;
import net.aminecraftdev.discordbot.message.interfaces.*;
import net.aminecraftdev.discordbot.message.*;
import net.aminecraftdev.discordbot.number.*;
import net.aminecraftdev.discordbot.role.*;
import net.aminecraftdev.discordbot.role.interfaces.*;
import net.aminecraftdev.discordbot.service.interfaces.*;
import net.aminecraftdev.discordbot.service.*;
import net.aminecraftdev.discordbot.spigot.interfaces.*;
import net.aminecraftdev.discordbot.spigot.*;
import net.aminecraftdev.discordbot.user.data.*;
import net.aminecraftdev.discordbot.user.factory.*;
import net.aminecraftdev.discordbot.user.interfaces.*;
import net.aminecraftdev.discordbot.user.*;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class CompositeRoot {

    public CompositeRoot() {
        EasyDI easyDI = new EasyDI();

        Bootstrap(easyDI);
        BootstrapCoR(easyDI);
        RunApplication(easyDI);

    }

    public void RunApplication(EasyDI easyDI) {
        IApplicationActivator applicationActivator = easyDI.getInstance(IApplicationActivator.class);
        ISpigotActivator spigotActivator = easyDI.getInstance(ISpigotActivator.class);
        IDatabaseActivator databaseActivator = easyDI.getInstance(IDatabaseActivator.class);

        Thread applicationThread = new Thread(applicationActivator);
        Thread spigotThread = new Thread(spigotActivator);
        Thread databaseThread = new Thread(databaseActivator);

        applicationThread.start();
        spigotThread.start();
        databaseThread.start();
    }

    private void Bootstrap(EasyDI easyDI) {
        //Application activator
        easyDI.bindInterface(IApplicationActivator.class, ApplicationActivator.class);
        easyDI.bindInterface(ISpigotActivator.class, SpigotActivator.class);

        //Number Service
        easyDI.bindInterface(INumberHandler.class, NumberHandler.class);

        //Comparators
        easyDI.bindInterface(IChannelComparator.class, ChannelComparator.class);

        //Discord service
        easyDI.bindInterface(IDiscordService.class, DiscordService.class);
        easyDI.bindInterface(IDiscordUserContainer.class, DiscordUserContainer.class);
        easyDI.bindInterface(ISpigotPurchasedResources.class, SpigotPurchasedResources.class);
        easyDI.bindInterface(ISpigotResourceUpdate.class, SpigotResourceUpdate.class);

        //Authentication services
        easyDI.bindInterface(IAuthenticationFactory.class, AuthenticationFactory.class);
        easyDI.bindInterface(IAuthentication.class, Authentication.class);
        easyDI.bindInterface(ISpigotUserContainer.class, SpigotUserContainer.class);

        //JDA services
        easyDI.bindInterface(IJdaProvider.class, JdaProvider.class);

        //Logging services
        easyDI.bindInterface(IBotLogsLogger.class, BotLogsLogger.class);
        easyDI.bindInterface(IAlertsLogger.class, AlertsLogger.class);
        easyDI.bindInterface(IGiveawayLogger.class, GiveawayLogger.class);
        easyDI.bindInterface(IScoreboardLogger.class, ScoreboardLogger.class);

        //File services
        easyDI.bindInterface(IFileHandler.class, FileHandler.class);
        easyDI.bindInterface(ISettingsPathFactory.class, SettingsPathFactory.class);
        easyDI.bindInterface(ISettingsDataFactory.class, SettingsDataFactory.class);
        easyDI.bindInterface(ISettingsFileFactory.class, SettingsFileFactory.class);
        easyDI.bindInterface(ISettingsFile.class, SettingsFile.class);

        //Category services
        easyDI.bindInterface(ICategoryPermissionHandler.class, CategoryPermissionHandler.class);
        easyDI.bindInterface(ICategorySortHandler.class, CategorySortHandler.class);
        easyDI.bindInterface(ICategoryBuilder.class, CategoryBuilder.class);
        easyDI.bindInterface(ICategoryBuilderFactory.class, CategoryBuilderFactory.class);
        easyDI.bindInterface(ICategoryDeleteHandler.class, CategoryDeleteHandler.class);
        easyDI.bindInterface(ICategoryNameHandler.class, CategoryNameHandler.class);
        easyDI.bindInterface(ICategoryDescriptionHandler.class, CategoryDescriptionHandler.class);

        //Role services
        easyDI.bindInterface(IRoleDeleteHandler.class, RoleDeleteHandler.class);

        // ----------------- \\
        //                   \\
        // L I S T E N E R S \\
        //                   \\
        // ----------------- \\
        
        //Bad Word Listener services
        easyDI.bindInterface(IBadWordsContains.class, BadWordsContains.class);
        easyDI.bindInterface(IBadWordsService.class, BadWordsService.class);
        easyDI.bindInterface(IBadWordsHandler.class, BadWordsHandler.class);

        //Link Sharing Listener services
        easyDI.bindInterface(ILinkSharingWhitelistContains.class, LinkSharingWhitelistContains.class);
        easyDI.bindInterface(ILinkSharingContains.class, LinkSharingContains.class);
        easyDI.bindInterface(ILinkSharingService.class, LinkSharingService.class);
        easyDI.bindInterface(ILinkSharingHandler.class, LinkSharingHandler.class);

        //MessageHandler services
        easyDI.bindInterface(IMessageHandler.class, MessageHandler.class);
        easyDI.bindInterface(ICommandComparator.class, CommandComparator.class);

        //Listener services
        easyDI.bindInterface(IDiscordMessageFactory.class, DiscordMessageFactory.class);
        easyDI.bindInterface(IDiscordEventProvider.class, DiscordEventProvider.class);
        easyDI.bindInterface(IMessageContext.class, MessageContext.class);
        easyDI.bindInterface(IDiscordMessage.class, DiscordMessage.class);
        easyDI.bindInterface(ICommandHandlerContainer.class, CommandHandlerContainer.class);
        easyDI.bindInterface(ICommandsContainer.class, CommandsContainer.class);
        easyDI.bindInterface(IDiscordUserFactory.class, DiscordUserFactory.class);
        easyDI.bindInterface(IDiscordUserDataHandler.class, DiscordUserDataHandler.class);
        easyDI.bindInterface(IDiscordUser.class, DiscordUser.class);
        easyDI.bindInterface(ICommandHandler.class, CommandHandler.class);

        // --------------- \\
        //                 \\
        // C O M M A N D S \\
        //                 \\
        // --------------- \\

        //Alert Command Services
        easyDI.bindInterface(IAlertCommand.class, AlertCommand.class);
        easyDI.bindInterface(IAlertHandler.class, AlertHandler.class);
        easyDI.bindInterface(IAlertService.class, AlertService.class);
        easyDI.bindInterface(IAlertModel.class, AlertModel.class);

        //Confirm Command Services
        easyDI.bindInterface(IConfirmCommand.class, ConfirmCommand.class);
        easyDI.bindInterface(IConfirmModel.class, ConfirmModel.class);
        easyDI.bindInterface(IConfirmService.class, ConfirmService.class);
        easyDI.bindInterface(IConfirmHandler.class, ConfirmHandler.class);

        //Help Command Services
        easyDI.bindInterface(IHelpCommand.class, HelpCommand.class);
        easyDI.bindInterface(IHelpModel.class, HelpModel.class);
        easyDI.bindInterface(IHelpService.class, HelpService.class);
        easyDI.bindInterface(IHelpHandler.class, HelpHandler.class);

        //Login Command Services
        easyDI.bindInterface(ILoginCommand.class, LoginCommand.class);
        easyDI.bindInterface(ILoginModel.class, LoginModel.class);
        easyDI.bindInterface(ILoginService.class, LoginService.class);
        easyDI.bindInterface(ILoginHandler.class, LoginHandler.class);

        //Lookup Command Services
        easyDI.bindInterface(ILookupCommand.class, LookupCommand.class);
        easyDI.bindInterface(ILookupModel.class, LookupModel.class);
        easyDI.bindInterface(ILookupService.class, LookupService.class);
        easyDI.bindInterface(ILookupHandler.class, LookupHandler.class);

        //Plugin Command Services
        easyDI.bindInterface(IPluginCommand.class, PluginCommand.class);
        easyDI.bindInterface(IPluginModel.class, PluginModel.class);
        easyDI.bindInterface(IPluginService.class, PluginService.class);
        easyDI.bindInterface(IPluginHandler.class, PluginHandler.class);
        //Plugin SubCommand Services
        easyDI.bindInterface(IPluginCreateModel.class, PluginCreateModel.class);
        easyDI.bindInterface(IPluginDeleteModel.class, PluginDeleteModel.class);
        easyDI.bindInterface(IPluginLinkModel.class, PluginLinkModel.class);
        easyDI.bindInterface(IPluginUpdateModel.class, PluginUpdateModel.class);

        //Setup Command Services
        easyDI.bindInterface(ISetupCommand.class, SetupCommand.class);
        easyDI.bindInterface(ISetupModel.class, SetupModel.class);
        easyDI.bindInterface(ISetupService.class, SetupService.class);
        easyDI.bindInterface(ISetupHandler.class, SetupHandler.class);
        easyDI.bindInterface(ISetupImagePathFactory.class, SetupImagePathFactory.class);

        //Verify Command Services
        easyDI.bindInterface(IVerifyCommand.class, VerifyCommand.class);
        easyDI.bindInterface(IVerifyModel.class, VerifyModel.class);
        easyDI.bindInterface(IVerifyService.class, VerifyService.class);
        easyDI.bindInterface(IVerifyHandler.class, VerifyHandler.class);


        //Verify Confirm Key Storage service
        easyDI.bindInterface(IVerifyConfirmKeyContainer.class, VerifyConfirmKeyContainer.class);

        //database table values
        easyDI.bindInterface(IPluginDatabaseValues.class, PluginDatabaseValues.class);
        easyDI.bindInterface(IUserDatabaseValues.class, UserDatabaseValues.class);

        //storage services
        easyDI.bindInterface(IDatabaseActivator.class, DatabaseActivator.class);
        easyDI.bindInterface(IDatabaseAuthentication.class, DatabaseAuthentication.class);
        easyDI.bindInterface(IDatabase.class, HikariCP.class);

        // ------------------- \\
        //                     \\
        // S I N G L E T O N S \\
        //                     \\
        // ------------------- \\

        //Finally mark which ones are singleton
        easyDI.markAsSingleton(SetupImagePathFactory.class);
        easyDI.markAsSingleton(AuthenticationFactory.class);
        easyDI.markAsSingleton(DiscordUserFactory.class);
        easyDI.markAsSingleton(DiscordMessageFactory.class);
        easyDI.markAsSingleton(CategoryBuilderFactory.class);

        easyDI.markAsSingleton(FileHandler.class);
        easyDI.markAsSingleton(NumberHandler.class);
        easyDI.markAsSingleton(MessageHandler.class);
        easyDI.markAsSingleton(CommandHandler.class);
        easyDI.markAsSingleton(DiscordUserDataHandler.class);
        easyDI.markAsSingleton(MessageContext.class);

        easyDI.markAsSingleton(CategorySortHandler.class);
        easyDI.markAsSingleton(CategoryPermissionHandler.class);
        easyDI.markAsSingleton(CategoryDeleteHandler.class);
        easyDI.markAsSingleton(CategoryNameHandler.class);
        easyDI.markAsSingleton(CategoryDescriptionHandler.class);

        easyDI.markAsSingleton(RoleDeleteHandler.class);

        easyDI.markAsSingleton(ChannelComparator.class);
        easyDI.markAsSingleton(CommandComparator.class);

        easyDI.markAsSingleton(CommandHandlerContainer.class);
        easyDI.markAsSingleton(CommandsContainer.class);
        easyDI.markAsSingleton(SpigotUserContainer.class);
        easyDI.markAsSingleton(DiscordUserContainer.class);
        easyDI.markAsSingleton(VerifyConfirmKeyContainer.class);

        easyDI.markAsSingleton(DiscordEventProvider.class);
        easyDI.markAsSingleton(JdaProvider.class);

        easyDI.markAsSingleton(DatabaseAuthentication.class);
        easyDI.markAsSingleton(HikariCP.class);

        easyDI.markAsSingleton(PluginDatabaseValues.class);
        easyDI.markAsSingleton(UserDatabaseValues.class);
    }

    @SuppressWarnings("unchecked")
    private void BootstrapCoR(EasyDI easyDI) {
        List<IMessage> messageHandlers = easyDI.getInstance(ICommandHandlerContainer.class).getCommands();
        List<ICommand> commands = easyDI.getInstance(ICommandsContainer.class).getCommands();

        try {
            Field field = easyDI.getClass().getDeclaredField("interfaceMappings");

            field.setAccessible(true);

            Object object = field.get(easyDI);

            if(object instanceof Map) {
                Map<Class, Class> interfaceMappings = (Map<Class, Class>) object;

                for(Class<?> clazz : interfaceMappings.keySet()) {
                    boolean check = IMessage.class.isAssignableFrom(clazz);

                    if(check) {
                        Class<IMessage> messageHandler = (Class<IMessage>) clazz;
                        messageHandlers.add(easyDI.getInstance(messageHandler));
                    }

                    check = ICommand.class.isAssignableFrom(clazz);

                    if(check) {
                        Class<ICommand> command = (Class<ICommand>) clazz;

                        commands.add(easyDI.getInstance(command));
                    }
                }
            }

            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Queue<IMessage> queue = new LinkedList<>(messageHandlers);

        while(!queue.isEmpty()) {
            IMessage messageHandler = queue.poll();

            if(messageHandler == null) continue;

            if(queue.isEmpty()) {
                messageHandler.setSuccessor(null);
            } else {
                messageHandler.setSuccessor(queue.peek());
            }
        }

        easyDI.getInstance(IMessageContext.class).setMessageHandler(messageHandlers.get(0));
    }

}
