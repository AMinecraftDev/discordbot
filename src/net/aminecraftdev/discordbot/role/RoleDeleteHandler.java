package net.aminecraftdev.discordbot.role;

import net.aminecraftdev.discordbot.role.interfaces.IRoleDeleteHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Jun-19
 */
public class RoleDeleteHandler implements IRoleDeleteHandler {

    private IJdaProvider jdaProvider;

    public RoleDeleteHandler(IJdaProvider jdaProvider) {
        this.jdaProvider = jdaProvider;
    }

    @Override
    public void deleteRole(String pluginName, boolean ignoreCase) {
        Guild guild = this.jdaProvider.getDefaultGuild();
        List<Role> roles = guild.getRolesByName(pluginName, ignoreCase);

        roles.forEach(role -> role.delete().queue());
    }

}
