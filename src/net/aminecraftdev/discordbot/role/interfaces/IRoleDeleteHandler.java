package net.aminecraftdev.discordbot.role.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Jun-19
 */
public interface IRoleDeleteHandler {
    void deleteRole(String name, boolean ignoreCase);
}
