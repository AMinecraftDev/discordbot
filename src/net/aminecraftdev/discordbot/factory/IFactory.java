package net.aminecraftdev.discordbot.factory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IFactory<T> {

    T getInstance();

}
