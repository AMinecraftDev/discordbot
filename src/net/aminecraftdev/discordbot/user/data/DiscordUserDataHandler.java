package net.aminecraftdev.discordbot.user.data;

import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.users.IUserDatabaseValues;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public class DiscordUserDataHandler implements IDiscordUserDataHandler {

    private IUserDatabaseValues userDatabaseValues;
    private IDatabase database;

    public DiscordUserDataHandler(IUserDatabaseValues userDatabaseValues, IDatabase database) {
        this.userDatabaseValues = userDatabaseValues;
        this.database = database;
    }

    @Override
    public void setVerified(IDiscordUser discordUser, long spigotId, String generatedKey) {
        Map<String, Object> values = new HashMap<>();

        values.put(this.userDatabaseValues.getVerifiedDate(), System.currentTimeMillis());
        values.put(this.userDatabaseValues.getGeneratedKey(), generatedKey);
        values.put(this.userDatabaseValues.getSpigotId(), spigotId);

        setValues(discordUser, values);
    }

    @Override
    public long getVerifiedLong(IDiscordUser discordUser) {
        return (long) getValue(discordUser, this.userDatabaseValues.getVerifiedDate());
    }

    @Override
    public long getSpigotIdLong(IDiscordUser discordUser) {
        return (long) getValue(discordUser, this.userDatabaseValues.getSpigotId());
    }

    @Override
    public String getGeneratedKey(IDiscordUser discordUser) {
        return (String) getValue(discordUser, this.userDatabaseValues.getGeneratedKey());
    }

    @Override
    public boolean doesExist(IDiscordUser discordUser) {
        String tableName = this.userDatabaseValues.getTableName();
        String discordId = this.userDatabaseValues.getDiscordId();
        String sql = "SELECT * FROM " + tableName + " WHERE " + discordId + "=? LIMIT 1;";
        ResultSet resultSet = this.database.executeQuery(sql, discordUser.getDiscordId());

        try {
            return resultSet.next();
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public Object getValue(IDiscordUser discordUser, String key) {
        String tableName = this.userDatabaseValues.getTableName();
        String discordId = this.userDatabaseValues.getDiscordId();
        String sql = "SELECT " + key + " FROM " + tableName + " WHERE " + discordId + "=? LIMIT 1;";
        ResultSet resultSet = this.database.executeQuery(sql, discordUser.getDiscordId());

        try {
            if(!resultSet.next()) {
                return null;
            }

            return resultSet.getObject(key);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public void setValue(IDiscordUser discordUser, String key, Object object) {
        String tableName = this.userDatabaseValues.getTableName();
        String discordId = this.userDatabaseValues.getDiscordId();
        String sql = "INSERT INTO " + tableName + " (" + discordId + ", " + key + ") VALUES (?, ?) ON DUPLICATE KEY UPDATE " + key + " =?;";

        this.database.executeUpdate(sql, discordUser.getDiscordId(), object, object);
    }

    @Override
    public void setValues(IDiscordUser discordUser, Map<String, Object> objectMap) {
        String tableName = this.userDatabaseValues.getTableName();
        String discordId = this.userDatabaseValues.getDiscordId();

        StringBuilder columnBuilder = new StringBuilder("(");
        StringBuilder valuesBuilder = new StringBuilder("(");
        StringBuilder onDuplicateBuilder = new StringBuilder();

        columnBuilder.append(discordId);
        columnBuilder.append(", ");

        checkIfString(discordUser.getDiscordId(), valuesBuilder);
        valuesBuilder.append(", ");

        Iterator<Map.Entry<String, Object>> iterator = objectMap.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String string = entry.getKey();
            Object value = entry.getValue();

            columnBuilder.append("`");
            columnBuilder.append(string);
            columnBuilder.append("`");
            checkIfString(value, valuesBuilder);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append(string);
            onDuplicateBuilder.append("`");
            onDuplicateBuilder.append("=");
            checkIfString(value, onDuplicateBuilder);

            if(iterator.hasNext()) {
                columnBuilder.append(", ");
                valuesBuilder.append(", ");
                onDuplicateBuilder.append(", ");
            }
        }

        columnBuilder.append(")");
        valuesBuilder.append(")");

        String sql = "INSERT INTO " + tableName + " " + columnBuilder.toString() + " VALUES " + valuesBuilder.toString() + " ON DUPLICATE KEY UPDATE " + onDuplicateBuilder.toString() + ";";

        this.database.executeUpdate(sql);
    }

    private void checkIfString(Object value, StringBuilder stringBuilder) {
        if(value instanceof String) {
            stringBuilder.append("\"");
            stringBuilder.append(value);
            stringBuilder.append("\"");
        } else {
            stringBuilder.append(value);
        }
    }

}
