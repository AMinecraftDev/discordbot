package net.aminecraftdev.discordbot.user.data;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

import java.util.Map;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-19
 */
public interface IDiscordUserDataHandler {

    void setVerified(IDiscordUser discordUser, long spigotId, String generatedKey);

    long getVerifiedLong(IDiscordUser discordUser);

    long getSpigotIdLong(IDiscordUser discordUser);

    String getGeneratedKey(IDiscordUser discordUser);

    boolean doesExist(IDiscordUser discordUser);

    Object getValue(IDiscordUser discordUser, String key);

    void setValue(IDiscordUser discordUser, String key, Object object);

    void setValues(IDiscordUser discordUser, Map<String, Object> objectMap);
}
