package net.aminecraftdev.discordbot.user;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public enum Role {

    LEAD_DEVELOPER(1), //Resource Developer/Server Founder
    DEVELOPER(2), //Skilled Developer
    JR_DEVELOPER(10), //Beginner Developer

    PARTNER(3), //Server High-Authority Staff
    CLIENT(90), //Clients, people who we have done private work for
    SUPPORT(92), //Server Support Members, who are already Verified
    VERIFIED(95), //Verified Discord Members
    REGULAR(100); //Regular, new people

    private static Map<Integer, Role> ROLES = new TreeMap<>();

    private int priority;

    Role(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return this.priority;
    }

    public String getName() {
        return name().replace("_", " ");
    }

    public void addRole(IDiscordUser discordUser) {
        Member member = discordUser.getDiscordMember();

        if(member == null) return;

        List<net.dv8tion.jda.core.entities.Role> roles = getRoles(discordUser);

        roles.forEach(role -> {
            if(role.getName().equalsIgnoreCase(getName())) {
                member.getGuild().getController().addRolesToMember(member, role).queue();
            }
        });
    }

    public net.dv8tion.jda.core.entities.Role getDiscordRole(Guild guild) {
        List<net.dv8tion.jda.core.entities.Role> roles = guild.getRolesByName(getName(), true);

        return roles.stream().findFirst().orElse(null);
    }

    public static Role getRole(IDiscordUser discordUser) {
        List<net.dv8tion.jda.core.entities.Role> roles = getRoles(discordUser);

        for(Map.Entry<Integer, Role> entry : ROLES.entrySet()) {
            Role role = entry.getValue();

            for (net.dv8tion.jda.core.entities.Role activeRole : roles) {
                if(activeRole.getName().equalsIgnoreCase(role.getName())) return role;
            }
        }

        return REGULAR;
    }

    public static void SetupRoles() {
        ROLES.clear();

        for(Role role : values()) {
            ROLES.put(role.priority, role);
        }
    }

    private static List<net.dv8tion.jda.core.entities.Role> getRoles(IDiscordUser discordUser) {
        List<net.dv8tion.jda.core.entities.Role> roles;

        if(discordUser.getGuild() == null) {
            roles = discordUser.getDiscordMember().getRoles();
        } else {
            roles = discordUser.getGuild().getRoles();
        }

        return roles;
    }

}
