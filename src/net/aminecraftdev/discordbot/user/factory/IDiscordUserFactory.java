package net.aminecraftdev.discordbot.user.factory;

import net.aminecraftdev.discordbot.factory.IFactory;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface IDiscordUserFactory extends IFactory<IDiscordUser> {

}
