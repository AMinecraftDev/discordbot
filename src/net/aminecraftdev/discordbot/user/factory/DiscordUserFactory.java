package net.aminecraftdev.discordbot.user.factory;

import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.aminecraftdev.discordbot.user.data.IDiscordUserDataHandler;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class DiscordUserFactory implements IDiscordUserFactory {

    private IDiscordUserDataHandler discordUserDataHandler;
    private IJdaProvider jdaProvider;

    private IDiscordUser discordUser;

    public DiscordUserFactory(IDiscordUser discordUser, IJdaProvider jdaProvider, IDiscordUserDataHandler discordUserDataHandler) {
        this.discordUser = discordUser;

        this.discordUserDataHandler = discordUserDataHandler;
        this.jdaProvider = jdaProvider;
    }

    @Override
    public IDiscordUser getInstance() {
        try {
            return this.discordUser.getClass().getConstructor(IJdaProvider.class, IDiscordUserDataHandler.class).newInstance(this.jdaProvider, this.discordUserDataHandler);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
