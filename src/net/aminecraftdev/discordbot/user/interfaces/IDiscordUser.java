package net.aminecraftdev.discordbot.user.interfaces;

import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.user.Role;
import net.aminecraftdev.spigot.api.user.User;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.requests.RestAction;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public interface IDiscordUser {

    void loadFromDatabase();

    void setFakeRole(Role role);

    void addRole(Role role);

    Guild getGuild();

    RestAction<PrivateChannel> getPrivateMessage();

    void setDiscordMember(Member member);

    void setDiscordUser(net.dv8tion.jda.core.entities.User user);

    void setVerified(long spigotId);

    void setGeneratedKey(String newKey);

    String getJoinDate();

    String getVerifiedDate();

    Game getActiveGame();

    OnlineStatus getOnlineStatus();

    Role getRole();

    boolean isOwner();

    boolean isStaff();

    boolean isDeveloper();

    boolean isAtLeast(Role role);

    boolean isVerified();

    String getAsMention();

    String getDiscordName();

    long getDiscordId();

    Member getDiscordMember();

    net.dv8tion.jda.core.entities.User getDiscordUser();

    String getSpigotName();

    long getSpigotId();

    User getSpigotMember();

    String getGeneratedKey();
}
