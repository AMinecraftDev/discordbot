package net.aminecraftdev.discordbot.user;

import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.aminecraftdev.discordbot.user.data.IDiscordUserDataHandler;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.exceptions.PermissionException;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.requests.RestAction;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 21-Apr-18
 */
public class DiscordUser implements IDiscordUser {

    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm a (d/MM/yyyy)");

    private IDiscordUserDataHandler discordUserDataHandler;
    private boolean verified = false, loaded = false;
    private final IJdaProvider jdaProvider;
    private String generatedKey = null;
    private long spigotId = -1;
    private Role fakeRole;
    private Member member;
    private User user;

    public DiscordUser(IJdaProvider jdaProvider, IDiscordUserDataHandler discordUserDataHandler) {
        this.discordUserDataHandler = discordUserDataHandler;
        this.jdaProvider = jdaProvider;
    }

    @Override
    public void loadFromDatabase() {
        if(this.loaded) return;

        this.loaded = true;

        if(!doesExist()) return;

        String generatedKey = this.discordUserDataHandler.getGeneratedKey(this);
        long spigotId = this.discordUserDataHandler.getSpigotIdLong(this);

        this.verified = true;
        this.spigotId = spigotId;
        this.generatedKey = generatedKey;
    }

    @Override
    public void setFakeRole(Role role) {
        this.fakeRole = role;
    }

    @Override
    public void addRole(Role role) {
        role.addRole(this);
    }

    @Override
    public Guild getGuild() {
        return this.jdaProvider.getDefaultGuild();
    }

    @Override
    public RestAction<PrivateChannel> getPrivateMessage() {
        return this.user.openPrivateChannel();
    }

    @Override
    public void setDiscordMember(Member member) {
        this.member = member;
        this.user = member.getUser();
    }

    @Override
    public void setDiscordUser(User user) {
        this.user = user;
    }

    @Override
    public void setVerified(long spigotId) {
        this.spigotId = spigotId;
        this.verified = true;

        this.discordUserDataHandler.setVerified(this, spigotId, getGeneratedKey());
    }

    @Override
    public void setGeneratedKey(String newKey) {
        this.generatedKey = newKey;
    }

    @Override
    public String getJoinDate() {
        if(this.member == null) return "";

        Timestamp timestamp = Timestamp.valueOf(this.member.getJoinDate().atZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());

        return TIME_FORMAT.format(new Date(timestamp.getTime()));
    }

    @Override
    public String getVerifiedDate() {
        if(!isVerified()) return null;

        long verifiedLong = this.discordUserDataHandler.getVerifiedLong(this);

        return TIME_FORMAT.format(new Date(verifiedLong));
    }

    @Override
    public Game getActiveGame() {
        if(this.member == null) {
            return null;
        }

        return this.member.getGame();
    }

    @Override
    public OnlineStatus getOnlineStatus() {
        if(this.member == null) {
            return OnlineStatus.UNKNOWN;
        }

        return this.member.getOnlineStatus();
    }

    @Override
    public Role getRole() {
        if(this.fakeRole != null) return this.fakeRole;

        return Role.getRole(this);
    }

    @Override
    public boolean isOwner() {
        return getRole().getPriority() <= Role.LEAD_DEVELOPER.getPriority();
    }

    @Override
    public boolean isStaff() {
        return getRole().getPriority() <= Role.PARTNER.getPriority();
    }

    @Override
    public boolean isDeveloper() {
        if(getRole() == Role.PARTNER) return false;

        return getRole().getPriority() <= Role.JR_DEVELOPER.getPriority();
    }

    @Override
    public boolean isAtLeast(Role role) {
        return getRole().getPriority() <= role.getPriority();
    }

    @Override
    public boolean isVerified() {
        return this.verified;
    }

    @Override
    public String getAsMention() {
        return this.user.getAsMention();
    }

    @Override
    public String getDiscordName() {
        return this.user.getName();
    }

    @Override
    public long getDiscordId() {
        return this.user.getIdLong();
    }

    @Override
    public Member getDiscordMember() {
        return this.member;
    }

    @Override
    public User getDiscordUser() {
        return this.user;
    }

    @Override
    public String getSpigotName() {
        if(!isVerified()) return null;

        try {
            net.aminecraftdev.spigot.api.user.User spigotUser = SpigotSite.getAPI().getUserManager().getUserById((int) getSpigotId());

            return spigotUser.getUsername();
        } catch (ConnectionFailedException | PermissionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public long getSpigotId() {
        if(!isVerified()) return 0;

        if(this.spigotId == -1) {
            this.spigotId = this.discordUserDataHandler.getSpigotIdLong(this);
        }

        return this.spigotId;
    }

    @Override
    public net.aminecraftdev.spigot.api.user.User getSpigotMember() {
        try {
            return SpigotSite.getAPI().getUserManager().getUserById((int) getSpigotId());
        } catch (ConnectionFailedException | PermissionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String getGeneratedKey() {
        if(this.generatedKey == null) {
            this.generatedKey = this.discordUserDataHandler.getGeneratedKey(this);
        }

        return this.generatedKey;
    }

    private boolean doesExist() {
        return this.discordUserDataHandler.doesExist(this);
    }
}
