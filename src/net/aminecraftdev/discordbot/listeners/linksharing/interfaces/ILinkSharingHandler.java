package net.aminecraftdev.discordbot.listeners.linksharing.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ILinkSharingHandler extends IMessage {
}
