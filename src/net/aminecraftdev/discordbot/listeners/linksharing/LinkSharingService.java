package net.aminecraftdev.discordbot.listeners.linksharing;

import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingService;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingWhitelistContains;
import net.aminecraftdev.discordbot.logger.botlogs.IBotLogsLogger;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.ChannelType;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class LinkSharingService implements ILinkSharingService {

    private ILinkSharingWhitelistContains linkSharingWhitelistContains;
    private ICommandHandler commandHandler;
    private IBotLogsLogger botLogsLogger;

    public LinkSharingService(ILinkSharingWhitelistContains linkSharingWhitelistContains, ICommandHandler commandHandler, IBotLogsLogger botLogsLogger) {
        this.linkSharingWhitelistContains = linkSharingWhitelistContains;
        this.commandHandler = commandHandler;
        this.botLogsLogger = botLogsLogger;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        if(this.commandHandler.canHandle(discordMessage, this.linkSharingWhitelistContains)) return;
        if(discordMessage.getMessage().getChannelType() != ChannelType.TEXT) return;

        IDiscordUser discordUser = discordMessage.getDiscordUser();
        String guildId = discordMessage.getMessage().getGuild().getId();
        String contentRaw = discordMessage.getMessage().getContentRaw();
        String channelName = discordMessage.getMessage().getTextChannel().getAsMention();
        String discordUserName = discordUser.getAsMention();

        if(discordUser.isStaff()) return;

        this.commandHandler.deleteMessage(discordMessage);
        this.botLogsLogger.logMessage("**A message has been deleted from " + channelName + " by " + discordUserName + " which contained a link that wasn't whitelisted. Message as follows:**\n*" + contentRaw + "*", guildId);
    }
}
