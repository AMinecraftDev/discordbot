package net.aminecraftdev.discordbot.listeners.linksharing;

import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingContains;

import java.util.Arrays;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class LinkSharingContains implements ILinkSharingContains {

    @Override
    public List<String> getContains() {
        return Arrays.asList("https", "http", "www.");
    }
}
