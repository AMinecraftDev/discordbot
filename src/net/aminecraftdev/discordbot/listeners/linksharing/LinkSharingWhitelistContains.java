package net.aminecraftdev.discordbot.listeners.linksharing;

import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingWhitelistContains;

import java.util.Arrays;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class LinkSharingWhitelistContains implements ILinkSharingWhitelistContains {

    @Override
    public List<String> getContains() {
        return Arrays.asList("pastebin.com/", "hastebin.com/", "spigotmc.org/", "youtube.com/", "hasteb.in/", "paste.helpch.at/", "mc-market.org/", "gyazo.com/", "prntscr.com/", "imgur.com/");
    }
}
