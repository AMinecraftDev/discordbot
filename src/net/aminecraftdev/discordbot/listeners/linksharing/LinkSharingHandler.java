package net.aminecraftdev.discordbot.listeners.linksharing;


import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IContains;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IService;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingContains;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingHandler;
import net.aminecraftdev.discordbot.listeners.linksharing.interfaces.ILinkSharingService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class LinkSharingHandler implements ILinkSharingHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IContains contains;
    private IService service;

    public LinkSharingHandler(ICommandHandler commandHandler, ILinkSharingContains linkSharingContains, ILinkSharingService linkSharingService) {
        this.commandHandler = commandHandler;
        this.contains = linkSharingContains;
        this.service = linkSharingService;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        if(request.getMessage().getContentRaw().startsWith("!")) return false;

        return this.commandHandler.canHandle(request, this.contains);
    }

    @Override
    public IService getService() {
        return this.service;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
