package net.aminecraftdev.discordbot.listeners.badwords;

import net.aminecraftdev.discordbot.listeners.badwords.interfaces.IBadWordsContains;

import java.util.Arrays;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class BadWordsContains implements IBadWordsContains {

    @Override
    public List<String> getContains() {
        return Arrays.asList("sex", "dating", "bitch", "fuck", "slut", "whore", "cunt", "pussy", "faggot");
    }
}
