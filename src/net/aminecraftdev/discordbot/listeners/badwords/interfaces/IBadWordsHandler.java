package net.aminecraftdev.discordbot.listeners.badwords.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IBadWordsHandler extends IMessage {
}
