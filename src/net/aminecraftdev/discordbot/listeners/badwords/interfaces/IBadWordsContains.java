package net.aminecraftdev.discordbot.listeners.badwords.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IContains;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IBadWordsContains extends IContains {
}
