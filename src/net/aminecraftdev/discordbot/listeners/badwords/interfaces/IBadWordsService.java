package net.aminecraftdev.discordbot.listeners.badwords.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IBadWordsService extends IService {
}
