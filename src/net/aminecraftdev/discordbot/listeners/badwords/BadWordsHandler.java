package net.aminecraftdev.discordbot.listeners.badwords;


import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IContains;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IService;
import net.aminecraftdev.discordbot.listeners.badwords.interfaces.IBadWordsContains;
import net.aminecraftdev.discordbot.listeners.badwords.interfaces.IBadWordsHandler;
import net.aminecraftdev.discordbot.listeners.badwords.interfaces.IBadWordsService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public class BadWordsHandler implements IBadWordsHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IContains contains;
    private IService service;

    public BadWordsHandler(ICommandHandler commandHandler, IBadWordsContains badWordsContains, IBadWordsService badWordsService) {
        this.commandHandler = commandHandler;
        this.contains = badWordsContains;
        this.service = badWordsService;

    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.contains);
    }

    @Override
    public IService getService() {
        return this.service;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
