package net.aminecraftdev.discordbot.builder.category.factory;

import net.aminecraftdev.discordbot.builder.interfaces.ICategoryBuilder;
import net.aminecraftdev.discordbot.builder.interfaces.ICategoryBuilderFactory;
import net.aminecraftdev.discordbot.category.interfaces.ICategoryPermissionHandler;
import net.aminecraftdev.discordbot.category.interfaces.ICategorySortHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-May-19
 */
public class CategoryBuilderFactory implements ICategoryBuilderFactory {

    private final ICategoryPermissionHandler categoryPermissionHandler;
    private final ICategorySortHandler categorySortHandler;
    private final IJdaProvider jdaProvider;

    private ICategoryBuilder categoryBuilder;

    public CategoryBuilderFactory(ICategoryBuilder categoryBuilder, IJdaProvider jdaProvider, ICategorySortHandler categorySortHandler, ICategoryPermissionHandler categoryPermissionHandler) {
        this.categoryBuilder = categoryBuilder;

        this.categoryPermissionHandler = categoryPermissionHandler;
        this.categorySortHandler = categorySortHandler;
        this.jdaProvider = jdaProvider;
    }

    @Override
    public ICategoryBuilder getInstance() {
        try {
            return this.categoryBuilder.getClass().getConstructor(IJdaProvider.class, ICategorySortHandler.class, ICategoryPermissionHandler.class).newInstance(this.jdaProvider, this.categorySortHandler, this.categoryPermissionHandler);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }
}
