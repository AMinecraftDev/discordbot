package net.aminecraftdev.discordbot.builder.category;

import net.aminecraftdev.discordbot.builder.interfaces.ICategoryBuilder;
import net.aminecraftdev.discordbot.category.interfaces.ICategoryPermissionHandler;
import net.aminecraftdev.discordbot.category.interfaces.ICategorySortHandler;
import net.aminecraftdev.discordbot.message.interfaces.ICallback;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.managers.GuildController;

import java.util.*;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-May-19
 */
public class CategoryBuilder implements ICategoryBuilder {

    private final ICategoryPermissionHandler categoryPermissionHandler;
    private final ICategorySortHandler categorySortHandler;
    private final IJdaProvider jdaProvider;

    private Map<String, ICallback<Channel>> channelCallback = new HashMap<>();
    private boolean isPublic = true, sort = true, sortInnerChannels = true;
    private Map<String, Boolean> channelsToCreate = new HashMap<>();
    private List<Role> rolesThatCanAccess = new ArrayList<>();
    private String name = null;

    public CategoryBuilder(IJdaProvider jdaProvider, ICategorySortHandler categorySortHandler, ICategoryPermissionHandler categoryPermissionHandler) {
        this.jdaProvider = jdaProvider;
        this.categorySortHandler = categorySortHandler;
        this.categoryPermissionHandler = categoryPermissionHandler;
    }

    @Override
    public ICategoryBuilder setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public ICategoryBuilder setPublicCategory(boolean isPublic) {
        this.isPublic = isPublic;
        return this;
    }

    @Override
    public ICategoryBuilder setSortCategory(boolean sort) {
        this.sort = sort;
        return this;
    }

    @Override
    public ICategoryBuilder setSortCategoryChannels(boolean sortInnerChannels) {
        this.sortInnerChannels = sortInnerChannels;
        return this;
    }

    @Override
    public ICategoryBuilder setRoleCanAccess(Role role) {
        this.rolesThatCanAccess.add(role);
        return this;
    }

    @Override
    public ICategoryBuilder addChannel(String name, boolean locked) {
        this.channelsToCreate.put(name, locked);
        return this;
    }

    @Override
    public ICategoryBuilder addChannel(String name, boolean locked, ICallback<Channel> callback) {
        this.channelsToCreate.put(name, locked);
        this.channelCallback.put(name, callback);
        return this;
    }

    @Override
    public Category build(ICallback<Category> callback) {
        Category category = build();

        callback.call(category);

        return category;
    }

    @Override
    public Category build() {
        GuildController guildController = this.jdaProvider.getDefaultGuildController();
        Guild guild = this.jdaProvider.getDefaultGuild();
        Role publicRole = guild.getPublicRole();
        Role verifiedRole = guild.getRolesByName("Verified", false).stream().findFirst().orElse(null);
        List<Role> accessRoles = this.isPublic? Arrays.asList(verifiedRole) : this.rolesThatCanAccess;

        if(verifiedRole == null) {
            System.out.println("Could not find the verified role to set up the channels.");
            return null;
        }

        Category category = (Category) guildController.createCategory(this.name).complete();

        this.channelsToCreate.forEach((name, isLocked) -> {
                Channel channel = guildController.createTextChannel(name)
                        .setParent(category)
                        .complete();

                this.categoryPermissionHandler.removeAccessToChannel(channel, publicRole);

                if(isLocked) {
                    this.categoryPermissionHandler.applyAccessToViewChannel(channel, accessRoles);
                } else {
                    this.categoryPermissionHandler.applyBasicAccessToChannel(channel, accessRoles);
                }

                if(this.channelCallback.containsKey(name)) {
                    this.channelCallback.get(name).call(channel);
                }
            }
        );

        if(this.sortInnerChannels) this.categorySortHandler.sortInnerCategoryChannels(category, guildController);
        if(this.sort) this.categorySortHandler.sortCustomPluginCategories(guild, guildController);

        this.categoryPermissionHandler.removeAccessToChannel(category, publicRole);
        this.categoryPermissionHandler.applyBasicAccessToChannel(category, accessRoles);

        return category;
    }
}
