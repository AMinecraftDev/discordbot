package net.aminecraftdev.discordbot.builder.interfaces;

import net.aminecraftdev.discordbot.factory.IFactory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-May-19
 */
public interface ICategoryBuilderFactory extends IFactory<ICategoryBuilder> {

}
