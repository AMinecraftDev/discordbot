package net.aminecraftdev.discordbot.builder.interfaces;

import net.aminecraftdev.discordbot.builder.IBuilder;
import net.aminecraftdev.discordbot.message.interfaces.ICallback;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.Role;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-May-19
 */
public interface ICategoryBuilder extends IBuilder<Category> {

    ICategoryBuilder setName(String name);

    ICategoryBuilder setPublicCategory(boolean isPublic);

    ICategoryBuilder setSortCategory(boolean sort);

    ICategoryBuilder setSortCategoryChannels(boolean sortInnerChannels);

    ICategoryBuilder setRoleCanAccess(Role role);

    ICategoryBuilder addChannel(String name, boolean locked);

    ICategoryBuilder addChannel(String name, boolean locked, ICallback<Channel> callback);
}
