package net.aminecraftdev.discordbot.builder;

import net.aminecraftdev.discordbot.message.interfaces.ICallback;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-May-19
 */
public interface IBuilder<T> {

    T build();

    T build(ICallback<T> callback);

}
