package net.aminecraftdev.discordbot.logger;

import net.dv8tion.jda.core.entities.TextChannel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface ILogger {

    String getValue();

    String getValue(String guildId);

    boolean doesExist(String guildId);

    void logMessage(String message, String guildId);

    void logMessage(String message, TextChannel textChannel);
}
