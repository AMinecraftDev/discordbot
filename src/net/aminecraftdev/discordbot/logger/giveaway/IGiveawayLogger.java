package net.aminecraftdev.discordbot.logger.giveaway;

import net.aminecraftdev.discordbot.logger.ILogger;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Apr-19
 */
public interface IGiveawayLogger extends ILogger {
}
