package net.aminecraftdev.discordbot.logger.botlogs;

import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsFileFactory;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Apr-19
 */
public class BotLogsLogger implements IBotLogsLogger {

    private ISettingsFileFactory settingsFileFactory;
    private IMessageHandler messageHandler;
    private IJdaProvider jdaProvider;

    public BotLogsLogger(ISettingsFileFactory settingsFileFactory, IJdaProvider jdaProvider, IMessageHandler messageHandler) {
        this.settingsFileFactory = settingsFileFactory;
        this.messageHandler = messageHandler;
        this.jdaProvider = jdaProvider;
    }

    @Override
    public String getValue() {
        return "botLogsChannel";
    }

    @Override
    public String getValue(String guildId) {
        return getValue() + "." + guildId;
    }

    @Override
    public boolean doesExist(String guildId) {
        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();

        return settingsFile.getMapOfData().containsKey(getValue(guildId));
    }

    @Override
    public void logMessage(String message, String guildId) {
        if(!doesExist(guildId)) return;

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        long channelId = (long) settingsFile.getValue("botLogsChannel." + guildId);
        JDA jda = this.jdaProvider.getJDA();

        TextChannel textChannel = jda.getTextChannelById(channelId);

        logMessage(message, textChannel);
    }

    @Override
    public void logMessage(String message, TextChannel textChannel) {
        this.messageHandler.handleMessage(textChannel, message);
    }
}
