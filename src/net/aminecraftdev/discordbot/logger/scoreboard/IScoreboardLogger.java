package net.aminecraftdev.discordbot.logger.scoreboard;

import net.aminecraftdev.discordbot.logger.ILogger;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Apr-19
 */
public interface IScoreboardLogger extends ILogger {

}
