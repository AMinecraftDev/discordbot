package net.aminecraftdev.discordbot.logger.alerts;

import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsFileFactory;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.TextChannel;

import java.awt.*;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Apr-19
 */
public class AlertsLogger implements IAlertsLogger {

    private ISettingsFileFactory settingsFileFactory;
    private IMessageHandler messageHandler;
    private IJdaProvider jdaProvider;

    public AlertsLogger(ISettingsFileFactory settingsFileFactory, IJdaProvider jdaProvider, IMessageHandler messageHandler) {
        this.settingsFileFactory = settingsFileFactory;
        this.messageHandler = messageHandler;
        this.jdaProvider = jdaProvider;
    }

    @Override
    public String getValue() {
        return "alertsChannel";
    }

    @Override
    public String getValue(String guildId) {
        return getValue() + "." + guildId;
    }

    @Override
    public boolean doesExist(String guildId) {
        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();

        return settingsFile.getMapOfData().containsKey(getValue(guildId));
    }

    @Override
    public void logMessage(String message, String guildId) {
        if(!doesExist(guildId)) return;

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        long channelId = (long) settingsFile.getValue(getValue(guildId));
        JDA jda = this.jdaProvider.getJDA();

        TextChannel textChannel = jda.getTextChannelById(channelId);

        logMessage(message, textChannel);
    }

    @Override
    public void logMessage(String message, TextChannel textChannel) {
        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(255, 100, 0))
                .setTitle("   **! ! !** __IMPORTANT ANNOUNCEMENT__ **! ! !**   ")
                .setDescription(message);

        this.messageHandler.handleEmbedMessage(textChannel, embedBuilder);
        this.messageHandler.handleMessage(textChannel, "@everyone", true, 1);
    }
}
