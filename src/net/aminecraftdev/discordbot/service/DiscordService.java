package net.aminecraftdev.discordbot.service;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.service.interfaces.IDiscordService;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;

import javax.security.auth.login.LoginException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class DiscordService implements IDiscordService {

    @Override
    public JDA startBot(JDABuilder jdaBuilder, IAuthentication authentication) {
        jdaBuilder.setToken(authentication.getPassword());
        jdaBuilder.setAutoReconnect(true);
        jdaBuilder.setStatus(OnlineStatus.ONLINE);

        try {
            return jdaBuilder.buildBlocking();
        } catch (LoginException | InterruptedException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public void stopBot() {

    }


}
