package net.aminecraftdev.discordbot.service;

import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsFileFactory;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class JdaProvider implements IJdaProvider {

    private ISettingsFileFactory settingsFileFactory;
    private JDA jda;

    public JdaProvider(ISettingsFileFactory settingsFileFactory) {
        this.settingsFileFactory = settingsFileFactory;
    }

    @Override
    public JDA getJDA() {
        return jda;
    }

    @Override
    public Guild getDefaultGuild() {
        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        long guildId = (long) settingsFile.getValue("botGuild");

        return getJDA().getGuildById(guildId);
    }

    @Override
    public GuildController getDefaultGuildController() {
        return getDefaultGuild().getController();
    }

    @Override
    public void setJDA(JDA jda) {
        this.jda = jda;
    }

}
