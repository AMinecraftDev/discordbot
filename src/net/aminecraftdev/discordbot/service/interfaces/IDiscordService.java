package net.aminecraftdev.discordbot.service.interfaces;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IDiscordService {

    JDA startBot(JDABuilder jdaBuilder, IAuthentication authentication);

    void stopBot();

}
