package net.aminecraftdev.discordbot.service.interfaces;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.managers.GuildController;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IJdaProvider {

    JDA getJDA();

    Guild getDefaultGuild();

    GuildController getDefaultGuildController();

    void setJDA(JDA jda);
}
