package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpCommand;
import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpService;
import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpHandler;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 17-Apr-18
 */
public class HelpHandler implements IHelpHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IHelpService helpService;
    private ICommand command;

    public HelpHandler(IHelpService helpService, IHelpCommand helpCommand, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.helpService = helpService;
        this.command = helpCommand;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.helpService;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
