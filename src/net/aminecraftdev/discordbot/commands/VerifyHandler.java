package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyCommand;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyHandler;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public class VerifyHandler implements IVerifyHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IVerifyService verifyService;
    private ICommand command;

    public VerifyHandler(IVerifyService verifyService, IVerifyCommand command, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.command = command;
        this.verifyService = verifyService;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.verifyService;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
