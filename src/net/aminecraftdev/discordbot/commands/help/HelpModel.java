package net.aminecraftdev.discordbot.commands.help;

import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpModel;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.container.interfaces.ICommandsContainer;
import net.aminecraftdev.discordbot.message.interfaces.ICommandComparator;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public class HelpModel implements IHelpModel {

    private ICommandsContainer commandsContainer;
    private ICommandComparator commandComparator;

    private IMessageHandler messageHandler;

    public HelpModel(IMessageHandler messageHandler, ICommandsContainer commandsContainer, ICommandComparator commandComparator) {
        this.messageHandler = messageHandler;

        this.commandsContainer = commandsContainer;
        this.commandComparator = commandComparator;
    }

    @Override
    public void handleHelp(IDiscordMessage discordMessage) {
        IDiscordUser discordUser = discordMessage.getDiscordUser();
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();

        List<ICommand> commandList = new ArrayList<>(this.commandsContainer.getCommands());
        StringBuilder stringBuilder = new StringBuilder();

        commandList.stream().sorted(this.commandComparator).filter(command -> command.canAccessCommand(discordUser)).forEach(command -> stringBuilder.append(command.getCommandDescription()).append("\n\n"));

        this.messageHandler.handleMessage(messageChannel, stringBuilder.toString(), true, 30);
    }
}
