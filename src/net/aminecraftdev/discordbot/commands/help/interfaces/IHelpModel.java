package net.aminecraftdev.discordbot.commands.help.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public interface IHelpModel {

    void handleHelp(IDiscordMessage discordMessage);

}
