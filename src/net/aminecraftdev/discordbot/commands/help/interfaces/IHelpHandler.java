package net.aminecraftdev.discordbot.commands.help.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 17-Apr-18
 */
public interface IHelpHandler extends IMessage {
}
