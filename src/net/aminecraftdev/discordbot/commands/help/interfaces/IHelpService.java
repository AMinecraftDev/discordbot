package net.aminecraftdev.discordbot.commands.help.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 17-Apr-18
 */
public interface IHelpService extends IService {

}
