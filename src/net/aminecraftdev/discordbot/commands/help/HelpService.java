package net.aminecraftdev.discordbot.commands.help;

import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpModel;
import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 17-Apr-18
 */
public class HelpService implements IHelpService {

    private IHelpModel helpModel;

    public HelpService(IHelpModel helpModel) {
        this.helpModel = helpModel;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        this.helpModel.handleHelp(discordMessage);
    }
}
