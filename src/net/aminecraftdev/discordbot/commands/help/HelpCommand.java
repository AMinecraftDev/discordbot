package net.aminecraftdev.discordbot.commands.help;

import net.aminecraftdev.discordbot.commands.help.interfaces.IHelpCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class HelpCommand implements IHelpCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return true;
    }

    @Override
    public String getCommandDescription() {
        return "**!help** - View the list of commands you have access to and what each one does.";
    }

    @Override
    public String getPrefix() {
        return "!help";
    }
}
