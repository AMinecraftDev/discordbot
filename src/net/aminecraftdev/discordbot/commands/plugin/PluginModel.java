package net.aminecraftdev.discordbot.commands.plugin;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.*;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.dv8tion.jda.core.entities.MessageChannel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public class PluginModel implements IPluginModel {

    private IPluginCreateModel pluginCreateModel;
    private IPluginUpdateModel pluginUpdateModel;
    private IPluginDeleteModel pluginDeleteModel;
    private IPluginLinkModel pluginLinkModel;

    private IMessageHandler messageHandler;
    private ICommandHandler commandHandler;
    private ICommand command;

    public PluginModel(IMessageHandler messageHandler, ICommandHandler commandHandler, IPluginCommand pluginCommand, IPluginCreateModel pluginCreateModel,
                       IPluginDeleteModel pluginDeleteModel, IPluginLinkModel pluginLinkModel, IPluginUpdateModel pluginUpdateModel) {
        this.pluginCreateModel = pluginCreateModel;
        this.pluginDeleteModel = pluginDeleteModel;
        this.pluginUpdateModel = pluginUpdateModel;
        this.pluginLinkModel = pluginLinkModel;

        this.messageHandler = messageHandler;
        this.commandHandler = commandHandler;
        this.command = pluginCommand;
    }

    @Override
    public void handleNotEnoughArgs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String message = "You have entered an invalid amount of arguments. Please use: \n" + this.command.getCommandDescription();

        this.messageHandler.handleMessage(messageChannel, message, true, 15);
    }

    @Override
    public void handleCreateArg(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        String[] split = rawContent.split(" ");

        if(split.length == 5 || split.length == 4) {
            String name = split[1];
            String pluginId = split[2];
            String premium = split[3];
            String cost = split.length == 5? split[4] : null;

            if(this.pluginCreateModel.isNameTaken(discordMessage, name)) return;
            if(this.pluginCreateModel.isPluginIdTaken(discordMessage, pluginId)) return;

            this.pluginCreateModel.handlePluginCreate(discordMessage, name, pluginId, premium, cost);
        } else {
            this.pluginCreateModel.handleNotEnoughArgs(discordMessage);
        }
    }

    @Override
    public void handleDeleteArg(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        String[] split = rawContent.split(" ");

        if(split.length == 2) {
            String pluginId = split[1];

            if(this.pluginDeleteModel.isPluginIdInvalid(discordMessage, pluginId)) return;
            if(!this.pluginDeleteModel.isPluginAuthor(discordMessage, pluginId)) return;

            this.pluginDeleteModel.handlePluginDelete(discordMessage, pluginId);
        } else {
            this.pluginDeleteModel.handleNotEnoughArgs(discordMessage);
        }
    }

    @Override
    public void handleUpdateArg(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        String[] split = rawContent.split(" ");

        if(split.length == 3) {
            String pluginId = split[1];
            String version = split[2];

            if(this.pluginUpdateModel.isPluginIdInvalid(discordMessage, pluginId)) return;
            if(!this.pluginUpdateModel.isPluginAuthor(discordMessage, pluginId)) return;

            this.pluginUpdateModel.handlePluginUpdate(discordMessage, pluginId, version);
        } else {
            this.pluginUpdateModel.handleNotEnoughArgs(discordMessage);
        }
    }

    @Override
    public void handleLinkArg(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        String[] split = rawContent.split(" ");

        if(split.length == 3) {
            String pluginId = split[1];
            String link = split[2];

            if(this.pluginLinkModel.isPluginIdInvalid(discordMessage, pluginId)) return;
            if(!this.pluginLinkModel.isPluginAuthor(discordMessage, pluginId)) return;

            this.pluginLinkModel.handlePluginLink(discordMessage, pluginId, link);
        } else {
            this.pluginLinkModel.handleNotEnoughArgs(discordMessage);
        }
    }
}
