package net.aminecraftdev.discordbot.commands.plugin.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public interface IPluginModel {

    void handleNotEnoughArgs(IDiscordMessage discordMessage);

    void handleCreateArg(IDiscordMessage discordMessage);

    void handleDeleteArg(IDiscordMessage discordMessage);

    void handleUpdateArg(IDiscordMessage discordMessage);

    void handleLinkArg(IDiscordMessage discordMessage);

}
