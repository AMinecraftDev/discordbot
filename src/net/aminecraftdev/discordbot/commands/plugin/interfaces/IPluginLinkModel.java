package net.aminecraftdev.discordbot.commands.plugin.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 11-Jun-19
 */
public interface IPluginLinkModel {

    void handleNotEnoughArgs(IDiscordMessage discordMessage);

    boolean isPluginAuthor(IDiscordMessage discordMessage, String pluginId);

    boolean isPluginIdInvalid(IDiscordMessage discordMessage, String pluginId);

    void handlePluginLink(IDiscordMessage discordMessage, String pluginId, String link);
}
