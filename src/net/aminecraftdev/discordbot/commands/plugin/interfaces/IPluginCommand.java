package net.aminecraftdev.discordbot.commands.plugin.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public interface IPluginCommand extends ICommand {
}
