package net.aminecraftdev.discordbot.commands.plugin.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 07-Jul-19
 */
public interface IPluginSubCommandBase {

    void handleNotEnoughArgs(IDiscordMessage discordMessage);

    boolean isPluginAuthor(IDiscordMessage discordMessage, String pluginId);

    boolean isPluginIdInvalid(IDiscordMessage discordMessage, String pluginId);

}
