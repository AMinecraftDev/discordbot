package net.aminecraftdev.discordbot.commands.plugin.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public interface IPluginCreateModel {

    void handleNotEnoughArgs(IDiscordMessage discordMessage);

    boolean isNameTaken(IDiscordMessage discordMessage, String name);

    boolean isPluginIdTaken(IDiscordMessage discordMessage, String pluginId);

    void handlePluginCreate(IDiscordMessage discordMessage, String name, String pluginId, String premium, String cost);
}
