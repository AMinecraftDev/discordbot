package net.aminecraftdev.discordbot.commands.plugin;

import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public class PluginCommand implements IPluginCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return discordUser.isDeveloper();
    }

    @Override
    public String getCommandDescription() {
        return "**!plugin create [name] [pluginId] [premium (true/false)] (cost)** - Create's a plugin/category with the desired channels, a role if the plugin is premium, and it will check all current players in the discord to see if they have the plugin in their purchase list if it is an official spigot plugin.\n" +
                "**!plugin update [id] [version]** - Will post a plugin update in the designated #announcements\n" +
                "**!plugin delete [id]** - Will delete a plugin and all it's assigned categories, roles, etc.\n" +
                "**!plugin link [id] [link]** - Will set the link for the plugin which will be updated in the channel descriptions to appear above the channel.";
    }

    @Override
    public String getPrefix() {
        return "!plugin";
    }
}
