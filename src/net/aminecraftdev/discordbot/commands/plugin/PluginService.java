package net.aminecraftdev.discordbot.commands.plugin;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginModel;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public class PluginService implements IPluginService {

    private ICommandHandler commandHandler;
    private IPluginModel pluginModel;
    private ICommand command;

    public PluginService(IPluginModel pluginModel, IPluginCommand pluginCommand, ICommandHandler commandHandler) {
        this.command = pluginCommand;
        this.commandHandler = commandHandler;
        this.pluginModel = pluginModel;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        IDiscordUser discordUser = discordMessage.getDiscordUser();
        String[] split = rawContent.split(" ");

        if(!this.command.canAccessCommand(discordUser)) return;

        if(rawContent.equals("") || split.length <= 0) {
            this.pluginModel.handleNotEnoughArgs(discordMessage);
            return;
        }

        String arg = split[0];

        if(arg.equalsIgnoreCase("create")) {
            this.pluginModel.handleCreateArg(discordMessage);
        } else if (arg.equalsIgnoreCase("delete")) {
            this.pluginModel.handleDeleteArg(discordMessage);
        } else if (arg.equalsIgnoreCase("link")) {
            this.pluginModel.handleLinkArg(discordMessage);
        } else if (arg.equalsIgnoreCase("update")) {
            this.pluginModel.handleUpdateArg(discordMessage);
        } else {
            this.pluginModel.handleNotEnoughArgs(discordMessage);
        }
    }
}
