package net.aminecraftdev.discordbot.commands.plugin;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginSubCommandBase;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.aminecraftdev.discordbot.user.Role;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 07-Jul-19
 */
public class PluginSubCommandBase implements IPluginSubCommandBase {

    private IPluginDatabaseValues databaseValues;
    private IDatabase database;

    private IMessageHandler messageHandler;
    private INumberHandler numberHandler;
    private ICommand command;

    public PluginSubCommandBase(IPluginDatabaseValues pluginDatabaseValues, IDatabase database, IMessageHandler messageHandler, INumberHandler numberHandler, IPluginCommand pluginCommand) {
        this.databaseValues = pluginDatabaseValues;
        this.database = database;

        this.messageHandler = messageHandler;
        this.numberHandler = numberHandler;

        this.command = pluginCommand;
    }

    @Override
    public void handleNotEnoughArgs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String message = "You have entered an invalid amount of arguments. Please use: \n" + this.command.getCommandDescription();

        this.messageHandler.handleMessage(messageChannel, message, true, 10);
    }

    @Override
    public boolean isPluginAuthor(IDiscordMessage discordMessage, String pluginId) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String authorId = discordMessage.getDiscordUser().getSpigotId()+"";

        if(isPluginIdInvalid(discordMessage, pluginId)) return false;

        ResultSet resultSet = this.database.selectAllFrom(this.databaseValues.getPluginId(), pluginId, this.databaseValues);
        Long authorIdForPlugin;

        try {
            authorIdForPlugin = resultSet.getLong(this.databaseValues.getAuthorId());
        } catch (SQLException ex) {
            String message = "Something went wrong while trying to find the plugin author id from the database.";

            this.messageHandler.handleMessage(messageChannel, message, true, 3);
            return false;
        }

        if(!authorId.equals(authorIdForPlugin.toString())) {
            if(discordMessage.getDiscordUser().getRole() == Role.LEAD_DEVELOPER) return true;

            String message = "You do not have the same author ID as the person who created this plugin. Only the person who created this plugin or a lead developer can update the link to it.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return false;
        }

        return true;
    }

    @Override
    public boolean isPluginIdInvalid(IDiscordMessage discordMessage, String pluginId) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);

        if(!this.numberHandler.isInt(pluginId)) {
            String message = "The pluginId you have specified is not a valid number, please try again with another id.";

            this.messageHandler.handleMessage(messageChannel, message, true, 3);
            return true;
        }

        if(!this.database.doesExist(this.databaseValues.getPluginId(), pluginId, this.databaseValues)) {
            String message = "That plugin id does not exist. To check out the plugin id of the plugin you're trying to delete and check out the documentation channel description.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        return false;
    }
}
