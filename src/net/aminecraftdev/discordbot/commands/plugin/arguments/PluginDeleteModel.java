package net.aminecraftdev.discordbot.commands.plugin.arguments;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryDeleteHandler;
import net.aminecraftdev.discordbot.category.interfaces.ICategoryNameHandler;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.plugin.PluginSubCommandBase;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginDeleteModel;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.logger.botlogs.IBotLogsLogger;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.aminecraftdev.discordbot.role.interfaces.IRoleDeleteHandler;
import net.aminecraftdev.discordbot.user.Role;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class PluginDeleteModel extends PluginSubCommandBase implements IPluginDeleteModel {

    private IPluginDatabaseValues databaseValues;
    private IDatabase database;

    private IMessageHandler messageHandler;

    private IBotLogsLogger botLogsLogger;

    private ICategoryDeleteHandler categoryDeleteHandler;
    private ICategoryNameHandler categoryNameHandler;
    private IRoleDeleteHandler roleDeleteHandler;

    public PluginDeleteModel(IPluginDatabaseValues pluginDatabaseValues, IDatabase database, IMessageHandler messageHandler, INumberHandler numberHandler, IPluginCommand pluginCommand,
                             IBotLogsLogger botLogsLogger, ICategoryDeleteHandler categoryDeleteHandler, ICategoryNameHandler categoryNameHandler, IRoleDeleteHandler roleDeleteHandler) {
        super(pluginDatabaseValues, database, messageHandler, numberHandler, pluginCommand);

        this.databaseValues = pluginDatabaseValues;
        this.database = database;

        this.messageHandler = messageHandler;

        this.botLogsLogger = botLogsLogger;

        this.categoryDeleteHandler = categoryDeleteHandler;
        this.categoryNameHandler = categoryNameHandler;
        this.roleDeleteHandler = roleDeleteHandler;
    }

    @Override
    public void handlePluginDelete(IDiscordMessage discordMessage, String pluginId) {
        ResultSet resultSet = this.database.selectAllFrom(this.databaseValues.getPluginId(), pluginId, this.databaseValues);
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String getAsMention = discordMessage.getDiscordUser().getAsMention();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        String pluginName;

        try {
            pluginName = resultSet.getString(this.databaseValues.getPluginName());
        } catch (SQLException e) {
            e.printStackTrace();

            this.messageHandler.handleMessage(messageChannel, "Failed to find pluginId in database, so couldn't delete the plugin.", true, 3);
            return;
        }

        String categoryName = this.categoryNameHandler.getCategoryName(pluginName);

        this.roleDeleteHandler.deleteRole(pluginName, false);
        this.categoryDeleteHandler.deleteCategory(categoryName, false, true);
        this.database.deleteFrom(this.databaseValues.getPluginId(), pluginId, this.databaseValues);

        String message = "You have deleted the plugin " + pluginName + " from our databases and the discord server.";

        this.botLogsLogger.logMessage(getAsMention + " has deleted the plugin with the id " + pluginId + " (" + pluginName + ") from the discord/database.", guildId);
        this.messageHandler.handleMessage(messageChannel, message, true, 10);
    }
}
