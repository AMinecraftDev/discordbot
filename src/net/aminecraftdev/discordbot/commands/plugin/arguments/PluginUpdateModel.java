package net.aminecraftdev.discordbot.commands.plugin.arguments;

import net.aminecraftdev.discordbot.commands.plugin.PluginSubCommandBase;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginUpdateModel;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.aminecraftdev.discordbot.spigot.interfaces.ISpigotResourceUpdate;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.ResultSet;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class PluginUpdateModel extends PluginSubCommandBase implements IPluginUpdateModel {

    private IPluginDatabaseValues databaseValues;
    private IDatabase database;

    private IMessageHandler messageHandler;

    private ISpigotResourceUpdate spigotResourceUpdate;

    public PluginUpdateModel(IPluginDatabaseValues pluginDatabaseValues, IDatabase database, IMessageHandler messageHandler, INumberHandler numberHandler, IPluginCommand pluginCommand,
                             ISpigotResourceUpdate spigotResourceUpdate) {
        super(pluginDatabaseValues, database, messageHandler, numberHandler, pluginCommand);

        this.databaseValues = pluginDatabaseValues;
        this.database = database;

        this.messageHandler = messageHandler;

        this.spigotResourceUpdate = spigotResourceUpdate;
    }

    @Override
    public void handlePluginUpdate(IDiscordMessage discordMessage, String pluginId, String version) {
        ResultSet resultSet = this.database.selectAllFrom(this.databaseValues.getPluginId(), pluginId, this.databaseValues);
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String pluginName;
        long updateChannelId;

        try {
            pluginName = resultSet.getString(this.databaseValues.getPluginName());
            updateChannelId = resultSet.getLong(this.databaseValues.getUpdateChannelId());
        } catch (Exception e) {
            e.printStackTrace();

            this.messageHandler.handleMessage(messageChannel, "Failed to find pluginId in database, so couldn't delete the plugin.", true, 3);
            return;
        }

        MessageChannel updateChannel = messageChannel.getJDA().getTextChannelById(updateChannelId);
        String link = this.spigotResourceUpdate.getSpigotResourceUpdateLink(Integer.valueOf(pluginId), pluginName);

        this.messageHandler.handleMessage(messageChannel, "Sending update for the specified plugin in message channel now.", true, 10);
        this.messageHandler.handleMessage(updateChannel, "Version **" + version + "** is now available for **" + pluginName  + "** which can be obtained at: " + link);

        //TODO: Handle Pinging
    }
}
