package net.aminecraftdev.discordbot.commands.plugin.arguments;

import net.aminecraftdev.discordbot.category.interfaces.ICategoryDescriptionHandler;
import net.aminecraftdev.discordbot.category.interfaces.ICategoryNameHandler;
import net.aminecraftdev.discordbot.commands.plugin.PluginSubCommandBase;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginLinkModel;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class PluginLinkModel extends PluginSubCommandBase implements IPluginLinkModel {

    private ICategoryDescriptionHandler categoryDescriptionHandler;
    private ICategoryNameHandler categoryNameHandler;

    private IPluginDatabaseValues databaseValues;
    private IDatabase database;

    private IMessageHandler messageHandler;

    public PluginLinkModel(IPluginDatabaseValues pluginDatabaseValues, IDatabase database, IMessageHandler messageHandler, INumberHandler numberHandler, IPluginCommand pluginCommand,
                           ICategoryDescriptionHandler categoryDescriptionHandler, ICategoryNameHandler categoryNameHandler) {
        super(pluginDatabaseValues, database, messageHandler, numberHandler, pluginCommand);

        this.categoryDescriptionHandler = categoryDescriptionHandler;
        this.categoryNameHandler = categoryNameHandler;

        this.databaseValues = pluginDatabaseValues;
        this.database = database;

        this.messageHandler = messageHandler;
    }

    @Override
    public void handlePluginLink(IDiscordMessage discordMessage, String pluginId, String link) {
        ResultSet resultSet = this.database.selectAllFrom(this.databaseValues.getPluginId(), pluginId, this.databaseValues);
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String pluginName;

        try {
            pluginName = resultSet.getString(this.databaseValues.getPluginName());
        } catch (SQLException e) {
            e.printStackTrace();

            this.messageHandler.handleMessage(messageChannel, "Failed to find pluginId in database, so couldn't delete the plugin.", true, 3);
            return;
        }

        String categoryName = this.categoryNameHandler.getCategoryName(pluginName);

        link = "**(" + pluginId + ")** " + link;

        this.categoryDescriptionHandler.updateChannelDescriptions(categoryName, false, link);
    }

}
