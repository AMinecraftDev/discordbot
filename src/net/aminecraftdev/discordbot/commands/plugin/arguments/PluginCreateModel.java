package net.aminecraftdev.discordbot.commands.plugin.arguments;

import net.aminecraftdev.discordbot.builder.interfaces.ICategoryBuilder;
import net.aminecraftdev.discordbot.builder.interfaces.ICategoryBuilderFactory;
import net.aminecraftdev.discordbot.category.interfaces.ICategoryNameHandler;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.plugin.PluginSubCommandBase;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCreateModel;
import net.aminecraftdev.discordbot.comparator.interfaces.IChannelComparator;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.plugins.IPluginDatabaseValues;
import net.aminecraftdev.discordbot.logger.botlogs.IBotLogsLogger;
import net.aminecraftdev.discordbot.message.interfaces.ICallback;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.aminecraftdev.discordbot.service.interfaces.IJdaProvider;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.requests.RestAction;
import net.dv8tion.jda.core.requests.restaction.order.ChannelOrderAction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class PluginCreateModel extends PluginSubCommandBase implements IPluginCreateModel {

    private IPluginDatabaseValues databaseValues;
    private IDatabase database;

    private ICategoryBuilderFactory categoryBuilderFactory;
    private ICategoryNameHandler categoryNameHandler;

    private IMessageHandler messageHandler;
    private INumberHandler numberHandler;
    private IJdaProvider jdaProvider;
    private IBotLogsLogger botLogsLogger;

    public PluginCreateModel(IPluginDatabaseValues pluginDatabaseValues, IDatabase database, IMessageHandler messageHandler, INumberHandler numberHandler, IPluginCommand pluginCommand,
                             ICategoryBuilderFactory categoryBuilderFactory, ICategoryNameHandler categoryNameHandler, IJdaProvider jdaProvider, IBotLogsLogger botLogsLogger) {
        super(pluginDatabaseValues, database, messageHandler, numberHandler, pluginCommand);

        this.databaseValues = pluginDatabaseValues;
        this.database = database;

        this.categoryBuilderFactory = categoryBuilderFactory;
        this.categoryNameHandler = categoryNameHandler;

        this.messageHandler = messageHandler;
        this.numberHandler = numberHandler;
        this.jdaProvider = jdaProvider;

        this.botLogsLogger = botLogsLogger;
    }

    @Override
    public boolean isNameTaken(IDiscordMessage discordMessage, String name) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String getAsMention = discordMessage.getDiscordUser().getAsMention();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        if(this.database.doesExist(this.databaseValues.getPluginName(), name, this.databaseValues)) {
            String message = "There is already a plugin registered to the database with this plugin name. Please rename your plugin to something more unique, or if this is an error get an admin to remove the plugin from the database.";

            this.botLogsLogger.logMessage(getAsMention + " has tried to create a plugin with the name " + name + " which is already registered to the database.", guildId);
            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        return false;
    }

    @Override
    public boolean isPluginIdTaken(IDiscordMessage discordMessage, String pluginId) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String getAsMention = discordMessage.getDiscordUser().getAsMention();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        if(!this.numberHandler.isInt(pluginId)) {
            String message = "The pluginId you have specified is not a valid number, please try again with another id.";

            this.messageHandler.handleMessage(messageChannel, message, true, 3);
            return true;
        }

        if(this.database.doesExist(this.databaseValues.getPluginId(), pluginId, this.databaseValues)) {
            String message = "There is already a plugin registered to the database with this plugin id. If this is an error get an admin to remove the plugin from the database.";

            this.botLogsLogger.logMessage(getAsMention + " has tried to create a plugin with the id " + pluginId + " which is already registered to the database.", guildId);
            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        return false;
    }

    @Override
    public void handlePluginCreate(IDiscordMessage discordMessage, String name, String pluginId, String premium, String cost) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        IDiscordUser discordUser = discordMessage.getDiscordUser();
        int pluginIdValue = this.numberHandler.getInt(pluginId);
        boolean isPremium;

        if(premium.equals("true") || premium.equals("false")) {
            isPremium = Boolean.valueOf(premium);
        } else {
            String message = "You must enter either **true** or **false** for the premium option on the createplugin command.";

            this.messageHandler.handleMessage(messageChannel, message, true, 3);
            return;
        }

        Double price = null;

        if(isPremium) {
            if(cost == null || !this.numberHandler.isDouble(cost)) {
                String message = "You have specified the plugin as a premium plugin, therefore you must also attach a cost to the plugin which has to be a valid double.";

                this.messageHandler.handleMessage(messageChannel, message, true, 3);
                return;
            }

            double priceValue = this.numberHandler.getDouble(cost);

            if(priceValue < 0.01) {
                String message = "If your plugin is listed for less than 1 cent, is it really worth selling?";

                this.messageHandler.handleMessage(messageChannel, message, true, 3);
                return;
            }

            price = priceValue;
        }

        GuildController guildController = this.jdaProvider.getDefaultGuildController();
        ICategoryBuilder categoryBuilder = this.categoryBuilderFactory.getInstance();
        String categoryName = this.categoryNameHandler.getCategoryName(name);
        AtomicLong updateChannelId = new AtomicLong();
        double finalPrice = price!=null? price : 0;

        categoryBuilder
                .setName(categoryName)
                .setSortCategory(true)
                .setSortCategoryChannels(true);

        if(isPremium) {
            Role role = guildController.createRole()
                    .setName(name)
                    .setMentionable(false)
                    .complete();

            categoryBuilder
                    .setPublicCategory(false)
                    .setRoleCanAccess(role);
        }

        categoryBuilder
                .addChannel("documentation", true)
                .addChannel("updates", true, channel -> updateChannelId.set(channel.getIdLong()))
                .addChannel("bugs", false)
                .addChannel("support", false)
                .addChannel("suggestions", false);

        ICallback<Category> callback = call -> this.database.executeUpdate(this.databaseValues.getInsertString(name, pluginIdValue, isPremium, finalPrice, discordUser.getSpigotId(), updateChannelId.get()));

        categoryBuilder.build(callback);
        //TODO: Check all current users for plugin
    }

}
