package net.aminecraftdev.discordbot.commands.interfaces;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jan-19
 */
public interface IContains {

    List<String> getContains();

}
