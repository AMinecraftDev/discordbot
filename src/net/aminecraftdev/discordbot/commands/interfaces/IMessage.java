package net.aminecraftdev.discordbot.commands.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IMessage extends IHandler<IDiscordMessage> {

}
