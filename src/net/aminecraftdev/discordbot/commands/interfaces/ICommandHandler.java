package net.aminecraftdev.discordbot.commands.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.dv8tion.jda.core.entities.Message;

import java.util.function.Consumer;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public interface ICommandHandler {

    boolean canHandle(IDiscordMessage discordMessage, IContains contains);

    boolean canHandle(IDiscordMessage discordMessage, IPrefix prefix);

    IHandler<IDiscordMessage> handleCommand(IDiscordMessage discordMessage, IMessage message);

    String getTrimmedCommand(IDiscordMessage discordMessage, IPrefix prefix);

    void deleteMessage(IDiscordMessage discordMessage);

    void deleteMessage(Message message);

    void deleteMessage(Message message, Consumer<Void> consumer);
}
