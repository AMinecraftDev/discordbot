package net.aminecraftdev.discordbot.commands.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface IHandler<T> {

    IHandler<T> getSuccessor();

    void setSuccessor(IHandler<T> successor);

    boolean canHandle(T request);

    IService getService();

    IHandler<T> handle(T request);
}
