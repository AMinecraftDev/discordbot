package net.aminecraftdev.discordbot.commands.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public interface IPrefix {

    String getPrefix();

}
