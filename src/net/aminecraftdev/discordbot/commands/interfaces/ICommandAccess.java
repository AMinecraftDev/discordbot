package net.aminecraftdev.discordbot.commands.interfaces;

import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public interface ICommandAccess {

    boolean canAccessCommand(IDiscordUser discordUser);

}
