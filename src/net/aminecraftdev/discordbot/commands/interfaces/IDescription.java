package net.aminecraftdev.discordbot.commands.interfaces;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 14-Apr-19
 */
public interface IDescription {

    String getCommandDescription();

}
