package net.aminecraftdev.discordbot.commands.setup.factory;

import net.aminecraftdev.discordbot.factory.IFactory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public interface ISetupImagePathFactory extends IFactory<String> {

}
