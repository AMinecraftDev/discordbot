package net.aminecraftdev.discordbot.commands.setup.factory;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public class SetupImagePathFactory implements ISetupImagePathFactory {

    @Override
    public String getInstance() {
        return "D:\\Minecraft Java Projects\\Discord Bots\\DiscordBot\\images";
    }
}
