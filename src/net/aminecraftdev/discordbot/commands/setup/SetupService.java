package net.aminecraftdev.discordbot.commands.setup;

import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupCommand;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupModel;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public class SetupService implements ISetupService {

    private ICommandHandler commandHandler;
    private ISetupModel setupModel;
    private ISetupCommand setupCommand;

    public SetupService(ISetupModel setupModel, ISetupCommand setupCommand, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.setupCommand = setupCommand;
        this.setupModel = setupModel;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.setupCommand);
        IDiscordUser discordUser = discordMessage.getDiscordUser();

        if(!this.setupCommand.canAccessCommand(discordUser)) return;

        if(rawContent.equals("")) {
            this.setupModel.handleNoArgs(discordMessage);
            return;
        }

        String[] args = rawContent.split(" ");

        if(args[0].startsWith("alerts")) {
            this.setupModel.handleSetupAlerts(discordMessage);
        } else if(args[0].startsWith("botcommands")) {
            this.setupModel.handleSetupBotCommands(discordMessage);
        } else if(args[0].startsWith("botlogs")) {
            this.setupModel.handleSetupBotLogs(discordMessage);
        } else if(args[0].startsWith("devcommands")) {
            this.setupModel.handleSetupDevCommands(discordMessage);
        } else if(args[0].startsWith("giveaway")) {
            this.setupModel.handleSetupGiveaway(discordMessage);
        } else if(args[0].startsWith("guild")) {
            this.setupModel.handleSetupGuild(discordMessage);
        } else if(args[0].startsWith("information")) {
            this.setupModel.handleSetupInformation(discordMessage);
        } else if(args[0].startsWith("lobby")) {
            this.setupModel.handleSetupLobby(discordMessage);
        } else if(args[0].startsWith("roles")) {
            this.setupModel.handleSetupRoles(discordMessage);
        } else if(args[0].startsWith("rules")) {
            this.setupModel.handleSetupRules(discordMessage);
        } else if(args[0].startsWith("scoreboards")) {
            this.setupModel.handleSetupScoreboards(discordMessage);
        } else {
            this.setupModel.handleNoArgs(discordMessage);
        }
    }
}
