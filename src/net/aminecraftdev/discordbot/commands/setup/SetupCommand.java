package net.aminecraftdev.discordbot.commands.setup;

import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class SetupCommand implements ISetupCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return discordUser.isOwner();
    }

    @Override
    public String getCommandDescription() {
        return
                "**!setup alerts** - Used to set the channel for all alerts to go to.\n" +
                "**!setup botcommands** - Used to set the admin bot commands channel.\n" +
                "**!setup botlogs** - Used to set the bot logs channel.\n" +
                "**!setup devcommands** - Used to set the dev bot commands channel.\n" +
                "**!setup giveaway** - Used to set the giveaway message channel.\n" +
                "**!setup guild** - Used to set the default Guild for the bot.\n" +
                "**!setup information** - Used to set the information messages.\n" +
                "**!setup lobby** - Used to set the lobby message.\n" +
                "**!setup roles** - Used to set the roles bot message channel.\n" +
                "**!setup rules** - Used to set up the rules messages.\n" +
                "**!setup scoreboards** - Used to set up the scoreboards channel.";
    }

    @Override
    public String getPrefix() {
        return "!setup";
    }
}
