package net.aminecraftdev.discordbot.commands.setup.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public interface ISetupModel {

    void handleNoArgs(IDiscordMessage discordMessage);

    void handleSetupGuild(IDiscordMessage discordMessage);

    void handleSetupLobby(IDiscordMessage discordMessage);

    void handleSetupRoles(IDiscordMessage discordMessage);

    void handleSetupDevCommands(IDiscordMessage discordMessage);

    void handleSetupBotCommands(IDiscordMessage discordMessage);

    void handleSetupInformation(IDiscordMessage discordMessage);

    void handleSetupRules(IDiscordMessage discordMessage);

    void handleSetupBotLogs(IDiscordMessage discordMessage);

    void handleSetupAlerts(IDiscordMessage discordMessage);

    void handleSetupGiveaway(IDiscordMessage discordMessage);

    void handleSetupScoreboards(IDiscordMessage discordMessage);

}
