package net.aminecraftdev.discordbot.commands.setup.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public interface ISetupService extends IService {
}
