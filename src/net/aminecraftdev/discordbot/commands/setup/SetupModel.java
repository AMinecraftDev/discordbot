package net.aminecraftdev.discordbot.commands.setup;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.setup.factory.ISetupImagePathFactory;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupModel;
import net.aminecraftdev.discordbot.container.interfaces.ICommandsContainer;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsFileFactory;
import net.aminecraftdev.discordbot.file.factory.interfaces.ISettingsPathFactory;
import net.aminecraftdev.discordbot.file.handler.IFileHandler;
import net.aminecraftdev.discordbot.file.interfaces.ISettingsFile;
import net.aminecraftdev.discordbot.logger.alerts.IAlertsLogger;
import net.aminecraftdev.discordbot.logger.botlogs.IBotLogsLogger;
import net.aminecraftdev.discordbot.logger.giveaway.IGiveawayLogger;
import net.aminecraftdev.discordbot.logger.scoreboard.IScoreboardLogger;
import net.aminecraftdev.discordbot.message.interfaces.ICommandComparator;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.user.Role;
import net.aminecraftdev.discordbot.user.factory.IDiscordUserFactory;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public class SetupModel implements ISetupModel {

    private ISetupImagePathFactory setupImagePathFactory;
    private ISettingsFileFactory settingsFileFactory;
    private ISettingsPathFactory settingsPathFactory;

    private IScoreboardLogger scoreboardLogger;
    private IGiveawayLogger giveawayLogger;
    private IBotLogsLogger botLogsLogger;
    private IAlertsLogger alertsLogger;

    private ICommandsContainer commandsContainer;
    private ICommandComparator commandComparator;
    private IMessageHandler messageHandler;
    private IFileHandler fileHandler;

    private IDiscordUserFactory discordUserFactory;

    public SetupModel(IMessageHandler messageHandler, ISettingsFileFactory settingsFileFactory, IScoreboardLogger scoreboardLogger,
                      ISettingsPathFactory settingsPathFactory, IFileHandler fileHandler, ISetupImagePathFactory setupImagePathFactory,
                      IBotLogsLogger botLogsLogger, IAlertsLogger alertsLogger, IGiveawayLogger giveawayLogger, ICommandsContainer commandsContainer,
                      ICommandComparator commandComparator, IDiscordUserFactory discordUserFactory) {
        this.setupImagePathFactory = setupImagePathFactory;
        this.settingsFileFactory = settingsFileFactory;
        this.settingsPathFactory = settingsPathFactory;
        this.discordUserFactory = discordUserFactory;
        this.commandsContainer = commandsContainer;
        this.commandComparator = commandComparator;
        this.scoreboardLogger = scoreboardLogger;
        this.messageHandler = messageHandler;
        this.giveawayLogger = giveawayLogger;
        this.botLogsLogger = botLogsLogger;
        this.alertsLogger = alertsLogger;
        this.fileHandler = fileHandler;
    }

    @Override
    public void handleNoArgs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setTitle("━━━━━━━━Setup Help━━━━━━━━")
                .setColor(new Color(150, 59, 154))
                .setFooter("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━", null);

        String description = "This command is used to set up the default message/channels for each channel/action. To see what command to use for each sub-string, please use !help";

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 10);
    }

    //#instructions
    @Override
    public void handleSetupLobby(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        String description = "To get verified you must type **!verify [SpigotMC Name]** in the **#verify** channel. An example would be *!verify AMinecraftDev*.\n\n" +
                "Once you've typed that message you must then go to SpigotMC where you will find a message from **AMinecraftDev** with a code. Copy that code and in" +
                " the same channel on Discord type **!confirm [code]**.\n\n" +
                "If your code matches the one you received in your inbox you will then be added to the appropriate roles based on the plugins you have purchased.";

        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "getverified.png")).queue();
        this.messageHandler.handleMessage(messageChannel, description);
    }

    @Override
    public void handleSetupRoles(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        StringBuilder description = new StringBuilder();

        description.append("__**Developer Roles**__\n");
        description.append("**Lead Developer** - Resource Developer/Server Founder\n");
        description.append("**Developer** - Advanced/Well-Known Resource Developer\n");
        description.append("**Jr Developer** - Beginner Resource Developer\n");
        description.append("\n");
        description.append("__**Support Roles**__\n");
        description.append("**Partner** - People who have partnered their server to this discord network and will offer support with our plugins.\n");
        description.append("**Support** - People who volunteer to help out others with any plugin they have purchased.\n");
        description.append("\n");
        description.append("__**Donator Roles**__\n");
        description.append("**VIP** - Someone who has donated between $5USD - $20USD to this community.\n");
        description.append("**MVP** - Someone who has donated more then $20USD to this community.\n");
        description.append("\n");
        description.append("__**Other Non-Essential Roles**__\n");
        description.append("**Discord Bot** - Our friendly bots that keep the server running smoothly and securely.\n");
        description.append("**Leecher** - Someone who has been known for leaking plugins or using cracked plugins.\n");
        description.append("**Verified** - Someone who's spigot account has been verified.\n");
        description.append("\n");
        description.append("*If you would like to join the support team for our Minecraft/SpigotMC Network, then shoot one of the Lead Developers a private message or ask in general chat.");

        //send image
        this.messageHandler.handleMessage(messageChannel, description.toString());
    }

    //#dev-commands
    @Override
    public void handleSetupDevCommands(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        StringBuilder stringBuilder = new StringBuilder();

        fillStringBuilder(Role.JR_DEVELOPER, stringBuilder);

        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "commands.png")).queue();
        this.messageHandler.handleMessage(messageChannel, stringBuilder.toString());
    }

    @Override
    public void handleSetupBotCommands(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        StringBuilder stringBuilder = new StringBuilder();

        fillStringBuilder(Role.LEAD_DEVELOPER, stringBuilder);

        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "commands.png")).queue();
        this.messageHandler.handleMessage(messageChannel, stringBuilder.toString());
    }

    //#information
    @Override
    public void handleSetupInformation(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        String about =
                "**Moderation**\n" +
                "We have a single custom bot to keep things running nicely, so avoid spamming, posting invite links, etc. If you do so, your message may be deleted and you may be temporarily muted/banned from the discord server.\n" +
                "\n" +
                "If our human support team find you to be breaking the rules, you may be kicked or banned.\n" +
                "\n" +
                "**Need something?**\n" +
                "If you are in need of some assistance, our staff are here to help. Simply just go to the #support-discussion for the plugin you need help with and tag @Partner and they will do their best to take care of you and/or your issue.\n" +
                "\n" +
                "**Have a suggestion to make?**\n" +
                "If you have a plugin suggestion for one of the currently made plugins then find the #suggestions channel under the plugin category and post your suggestion there. If you have a custom plugin suggestion, please use #global-suggestions to post it.\n" +
                "\n" +
                "**Have a bug to report?**\n" +
                "Our quality service is provided on the terms of when people find bugs they report them to us Developers so we can patch them and in return make the plugin better for everyone. If you have found a bug please go to the #bug channel under the plugin category and post the bug in that channel.";

        String links =
                "**Test Server IP** - Coming soon.\n" +
                "**AMinecraftDev's Spigot** *(Lead Dev)* - https://goo.gl/bNkZH1 \n" +
                "**Minerbeef's Spigot** *(Lead Dev)* - https://goo.gl/wr2E6n \n" +
                "**RyujiX's Spigot** *(Lead Dev)* - https://goo.gl/SW4zsz \n" +
                "**DarkKnights22 Spigot** *(Jr Dev)* - http://bit.ly/2DozUAy\n" +
                "**Discord** - https://discord.gg/x4KAERJ";

        String notifications =
                "**Notifications**\n" +
                "If you would like to receive a notification on Discord whenever a plugin you have access to is updated, then click the green arrow reaction to this message and you will be added to the Notification role, and whenever a plugin is updated you will be notified.";

        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "about.png")).queue();
        this.messageHandler.handleMessage(messageChannel, about);

        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "links.png")).queue();
        this.messageHandler.handleMessage(messageChannel, links);

        //send img
        messageChannel.sendMessage(notifications).queue(message -> {
            message.addReaction(":white_check_mark:").queue();
        });
    }

    //#rules
    @Override
    public void handleSetupRules(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        String rules =
                "**1.)** Messages must be appropriate, this means avoid talking about mature topics or sending inappropriate links\n" +
                "\n" +
                "**2.)** Don’t spam. The bots will stop you from doing this for the most part, but avoid repeatedly tagging staff members, messaging them, etc as well. \n" +
                "\n" +
                "**3.)** Avoid setting inappropriate nicknames or avatars.\n" +
                "\n" +
                "**4.)** Harassment of anyone on the server will most likely result in being kicked or banned. This includes threats, releasing personal info, etc.\n" +
                "\n" +
                "**5.)** Excessive rule breaking will result in a permanent ban\n" +
                "\n" +
                "**6.)** Do not post in a different category looking for support for different plugin, keep each category plugin specific.\n" +
                "\n" +
                "**If you are intentionally making discord a bad experience for others, staff reserve the right to remove you from the server for any reason, even if not outlined here. We’re pretty chill here, and as long as you’re not trying to be annoying, you shouldn’t have to worry.**\n" +
                "\n" +
                "\n" +
                "**We hope you enjoy your stay!**";


        messageChannel.sendFile(new File(this.setupImagePathFactory.getInstance(), "rules.png")).queue();
        this.messageHandler.handleMessage(messageChannel, rules);
    }

    @Override
    public void handleSetupBotLogs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        long channelId = messageChannel.getIdLong();

        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(150, 59, 154));

        String description = "You have just updated the bot log channel to " + channelId + ".";

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        settingsFile.updateValue(this.botLogsLogger.getValue(guildId), channelId);
        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), settingsFile.getMapOfData());

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 40);
    }

    @Override
    public void handleSetupAlerts(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        long channelId = messageChannel.getIdLong();

        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(150, 59, 154));

        String description = "You have just updated the alerts channel to " + channelId + ".";

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        settingsFile.updateValue(this.alertsLogger.getValue(guildId), channelId);
        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), settingsFile.getMapOfData());

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 40);
    }

    @Override
    public void handleSetupGiveaway(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        long channelId = messageChannel.getIdLong();

        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(150, 59, 154));

        String description = "You have just updated the giveaway channel to " + channelId + ".";

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        settingsFile.updateValue(this.giveawayLogger.getValue(guildId), channelId);
        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), settingsFile.getMapOfData());

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 40);
    }

    @Override
    public void handleSetupScoreboards(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        long channelId = messageChannel.getIdLong();

        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(150, 59, 154));

        String description = "You have just updated the scoreboards channel to " + channelId + ".";

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();
        String guildId = this.messageHandler.getGuildId(discordMessage);

        settingsFile.updateValue(this.scoreboardLogger.getValue(guildId), channelId);
        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), settingsFile.getMapOfData());

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 40);

        //TODO: Handle implementation of the scoreboard
    }

    @Override
    public void handleSetupGuild(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        Guild guild = discordMessage.getMessage().getGuild();
        long channelId = guild.getIdLong();

        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setColor(new Color(150, 59, 154));

        String description = "You have just updated the bot guild to " + channelId + ".";

        ISettingsFile settingsFile = this.settingsFileFactory.getInstance();

        settingsFile.updateValue("botGuild", channelId);

        this.fileHandler.saveFile(this.settingsPathFactory.getInstance(), settingsFile.getMapOfData());

        embedBuilder.setDescription(description);
        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder, true, 40);
    }

    private void fillStringBuilder(Role role, StringBuilder stringBuilder) {
        List<ICommand> commandList = new ArrayList<>(this.commandsContainer.getCommands());
        IDiscordUser discordUser = this.discordUserFactory.getInstance();

        discordUser.setFakeRole(role);

        commandList.stream()
                .sorted(this.commandComparator)
                .filter(command -> command.canAccessCommand(discordUser))
                .forEach(command -> stringBuilder.append(command.getCommandDescription()).append("\n\n"));
    }
}
