package net.aminecraftdev.discordbot.commands.alert;

import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertCommand;
import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertModel;
import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertService;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.logger.alerts.IAlertsLogger;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 30-Mar-19
 */
public class AlertService implements IAlertService {

    private IMessageHandler messageHandler;
    private ICommandHandler commandHandler;
    private IAlertModel alertModel;
    private ICommand command;

    private IAlertsLogger alertsLogger;

    public AlertService(IAlertModel alertModel, IAlertCommand alertCommand, ICommandHandler commandHandler, IMessageHandler messageHandler, IAlertsLogger alertsLogger) {
        this.commandHandler = commandHandler;
        this.messageHandler = messageHandler;
        this.alertModel = alertModel;
        this.command = alertCommand;

        this.alertsLogger = alertsLogger;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        IDiscordUser discordUser = discordMessage.getDiscordUser();

        if(!this.command.canAccessCommand(discordUser)) return;

        if(rawContent.equalsIgnoreCase("")) {
            this.alertModel.handleNoArguments(discordMessage);
            return;
        }

        String guildId = this.messageHandler.getGuildId(discordMessage);

        if(!this.alertsLogger.doesExist(guildId)) {
            this.alertModel.handleNoChannelSet(discordMessage);
            return;
        }



        this.alertModel.handleAlertMessage(discordMessage, rawContent);
    }
}
