package net.aminecraftdev.discordbot.commands.alert;

import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertModel;
import net.aminecraftdev.discordbot.logger.alerts.IAlertsLogger;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.dv8tion.jda.core.entities.TextChannel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 30-Mar-19
 */
public class AlertModel implements IAlertModel {

    private IMessageHandler messageHandler;
    private IAlertsLogger alertsLogger;

    public AlertModel(IMessageHandler messageHandler, IAlertsLogger alertsLogger) {
        this.messageHandler = messageHandler;
        this.alertsLogger = alertsLogger;
    }

    @Override
    public void handleNoArguments(IDiscordMessage discordMessage) {
        String message = "To use this command you must attach a message after the !alert which will be sent to the alerts channel. You can include any message formatting, spacing, tabbing, anything that you would in a normal message. And you do not need to @ everyone as it will automatically do that.";

        TextChannel textChannel = discordMessage.getMessage().getTextChannel();

        this.messageHandler.handleMessage(textChannel, message, true, 5);
    }

    @Override
    public void handleNoChannelSet(IDiscordMessage discordMessage) {
        String message = "There is currently no Alerts channel set in the configuration file, to set this you must get a Lead Developer/Founder to type in !setup alerts in the alert channel.";

        TextChannel textChannel = discordMessage.getMessage().getTextChannel();

        this.messageHandler.handleMessage(textChannel, message, true, 5);
    }

    @Override
    public void handleAlertMessage(IDiscordMessage discordMessage, String rawContent) {
        this.alertsLogger.logMessage(rawContent, this.messageHandler.getGuildId(discordMessage));
    }
}
