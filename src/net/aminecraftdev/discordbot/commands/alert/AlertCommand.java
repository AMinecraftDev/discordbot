package net.aminecraftdev.discordbot.commands.alert;

import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public class AlertCommand implements IAlertCommand {

    @Override
    public String getCommandDescription() {
        return "**!alert [message]** - This will send a formatted message in to the global alerts channel.";
    }

    @Override
    public String getPrefix() {
        return "!alert";
    }

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return discordUser.isStaff();
    }
}
