package net.aminecraftdev.discordbot.commands.alert.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 30-Mar-19
 */
public interface IAlertModel {

    void handleNoArguments(IDiscordMessage discordMessage);

    void handleNoChannelSet(IDiscordMessage discordMessage);

    void handleAlertMessage(IDiscordMessage discordMessage, String rawContent);
}
