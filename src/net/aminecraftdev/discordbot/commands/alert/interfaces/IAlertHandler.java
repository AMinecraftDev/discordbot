package net.aminecraftdev.discordbot.commands.alert.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 02-Apr-19
 */
public interface IAlertHandler extends IMessage {

}
