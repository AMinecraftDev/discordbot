package net.aminecraftdev.discordbot.commands.alert.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 30-Mar-19
 */
public interface IAlertService extends IService {
}
