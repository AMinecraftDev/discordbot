package net.aminecraftdev.discordbot.commands.alert.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public interface IAlertCommand extends ICommand {
}
