package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmCommand;
import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmHandler;
import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmService;
import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public class ConfirmHandler implements IConfirmHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IService service;
    private ICommand command;

    public ConfirmHandler(IConfirmService confirmService, IConfirmCommand confirmCommand, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.command = confirmCommand;
        this.service = confirmService;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.service;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
