package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertCommand;
import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertHandler;
import net.aminecraftdev.discordbot.commands.alert.interfaces.IAlertService;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 30-Mar-19
 */
public class AlertHandler implements IAlertHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IAlertService alertService;
    private ICommand command;

    public AlertHandler(IAlertService alertService, IAlertCommand alertCommand, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.alertService = alertService;
        this.command = alertCommand;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.alertService;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
