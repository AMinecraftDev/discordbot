package net.aminecraftdev.discordbot.commands.login;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginModel;
import net.aminecraftdev.discordbot.container.interfaces.ISpigotUserContainer;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.user.exceptions.InvalidCredentialsException;
import net.aminecraftdev.spigot.api.user.exceptions.TwoFactorAuthenticationException;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public class LoginModel implements ILoginModel {

    private ISpigotUserContainer spigotUserContainer;
    private IMessageHandler messageHandler;

    public LoginModel(IMessageHandler messageHandler, ISpigotUserContainer spigotUserContainer) {
        this.messageHandler = messageHandler;
        this.spigotUserContainer = spigotUserContainer;
    }

    @Override
    public void handleNoArguments(IDiscordMessage discordMessage) {
        IDiscordUser discordUser = discordMessage.getDiscordUser();

        String message =
                "It is recommended for you to login in the PMs so that no one can see your message in the message history on the Discord Server.\n" +
                "To log in use the command **!login [username] [password] (2FAKey)**\n" +
                "The 2FA Key is optional, but if you have the 2FA Key enabled on your SpigotMC account you must input it here to successfully log in.";

        this.messageHandler.handleMessage(discordUser.getPrivateMessage(), message);
    }

    @Override
    public void handleArgumentMessage(IDiscordMessage discordMessage, IAuthentication authentication) {
        String message = "You have successfully connected your SpigotMC account to the bot. For security reasons I would recommend that you delete your !login message with your attached credentials.";

        try {
            this.spigotUserContainer.addUser(SpigotSite.getAPI().getUserManager().authenticate(authentication));
        } catch (ConnectionFailedException ex) {
            message = "Something went wrong and the bot could not connect to the SpigotSite.";
        } catch (TwoFactorAuthenticationException ex) {
            message = "You have got 2FA enabled on your SpigotMC account, and didn't provide a correct 2FA key.";
        } catch (InvalidCredentialsException ex) {
            message = "You have entered the invalid credentials to your account and it could not sign in.";
        }

        this.messageHandler.handleMessage(discordMessage.getDiscordUser().getPrivateMessage(), message);
    }
}
