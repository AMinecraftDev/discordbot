package net.aminecraftdev.discordbot.commands.login;

import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class LoginCommand implements ILoginCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return discordUser.isDeveloper();
    }

    @Override
    public String getCommandDescription() {
        return "**!login** - Will open a private chat with the bot where u can sign in to your SpigotMC account through the bot. *Make sure you have PMs open!*\n" +
               "**!login [username] [password]** - Will login with the attached username and password.\n" +
               "**!login [username] [password] [2faKey]** - Will login with the attached username, password and 2fa key.";
    }

    @Override
    public String getPrefix() {
        return "!login";
    }
}
