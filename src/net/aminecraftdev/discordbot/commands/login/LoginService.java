package net.aminecraftdev.discordbot.commands.login;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.authentication.factory.IAuthenticationFactory;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginCommand;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginModel;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.dv8tion.jda.core.entities.ChannelType;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 *
 * !login [username] [password] (2fa)
 */
public class LoginService implements ILoginService {

    private IAuthenticationFactory authenticationFactory;
    private ICommandHandler commandHandler;
    private ILoginModel loginModel;
    private ICommand command;

    public LoginService(ILoginModel loginModel, ILoginCommand loginPrefix, ICommandHandler commandHandler, IAuthenticationFactory authenticationFactory) {
        this.authenticationFactory = authenticationFactory;
        this.commandHandler = commandHandler;
        this.loginModel = loginModel;
        this.command = loginPrefix;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);

        IDiscordUser discordUser = discordMessage.getDiscordUser();
        String[] args = rawContent.split(" ");

        if(!this.command.canAccessCommand(discordUser)) return;

        if(rawContent.equalsIgnoreCase("")) {
            this.loginModel.handleNoArguments(discordMessage);
            return;
        }

        IAuthentication authentication = this.authenticationFactory.getInstance();

        //no 2FA key
        if(args.length == 2) {
            authentication.setUsername(args[0]);
            authentication.setPassword(args[1]);
        } else if(args.length == 3) {
            authentication.setUsername(args[0]);
            authentication.setPassword(args[1]);
            authentication.set2FAKey(args[2]);
        } else {
            this.loginModel.handleNoArguments(discordMessage);
            return;
        }

        this.loginModel.handleArgumentMessage(discordMessage, authentication);
    }
}
