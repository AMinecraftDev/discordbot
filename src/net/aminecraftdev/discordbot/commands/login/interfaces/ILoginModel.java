package net.aminecraftdev.discordbot.commands.login.interfaces;

import net.aminecraftdev.discordbot.authentication.IAuthentication;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public interface ILoginModel {

    void handleNoArguments(IDiscordMessage discordMessage);

    void handleArgumentMessage(IDiscordMessage discordMessage, IAuthentication authentication);

}
