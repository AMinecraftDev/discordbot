package net.aminecraftdev.discordbot.commands.login.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public interface ILoginHandler extends IMessage {

}
