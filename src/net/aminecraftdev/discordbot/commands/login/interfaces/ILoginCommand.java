package net.aminecraftdev.discordbot.commands.login.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public interface ILoginCommand extends ICommand {
}
