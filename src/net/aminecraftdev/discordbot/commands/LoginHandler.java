package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginCommand;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginHandler;
import net.aminecraftdev.discordbot.commands.login.interfaces.ILoginService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 06-Feb-19
 */
public class LoginHandler implements ILoginHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IService service;
    private ICommand command;

    public LoginHandler(ILoginService loginService, ILoginCommand command, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.service = loginService;
        this.command = command;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.service;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
