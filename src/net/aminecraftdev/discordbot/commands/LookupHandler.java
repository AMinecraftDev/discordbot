package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.interfaces.*;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupCommand;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupHandler;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class LookupHandler implements ILookupHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private ILookupService lookupService;
    private ICommand lookupCommand;

    public LookupHandler(ILookupService lookupService, ILookupCommand lookupPrefix, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.lookupService = lookupService;
        this.lookupCommand = lookupPrefix;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public IService getService() {
        return this.lookupService;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage discordMessage) {
        return this.commandHandler.handleCommand(discordMessage, this);
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.lookupCommand);
    }
}
