package net.aminecraftdev.discordbot.commands.confirm;

import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmModel;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.container.interfaces.IVerifyConfirmKeyContainer;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.spigot.interfaces.ISpigotPurchasedResources;
import net.aminecraftdev.discordbot.user.Role;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.resource.Resource;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public class ConfirmModel implements IConfirmModel {

    private IVerifyConfirmKeyContainer verifyConfirmKeyContainer;
    private ISpigotPurchasedResources spigotPurchasedResources;
    private ICommandHandler commandHandler;
    private IMessageHandler messageHandler;

    public ConfirmModel(ICommandHandler commandHandler, IMessageHandler messageHandler, IVerifyConfirmKeyContainer verifyConfirmKeyContainer, ISpigotPurchasedResources spigotPurchasedResources) {
        this.commandHandler = commandHandler;
        this.messageHandler = messageHandler;
        this.spigotPurchasedResources = spigotPurchasedResources;
        this.verifyConfirmKeyContainer = verifyConfirmKeyContainer;
    }

    @Override
    public void handleNoArgs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String message = "You must enter the key after your command. An example of this is **!confirm " + this.messageHandler.generateNewCaptcha() + "**";

        if(handleWrongChannel(discordMessage)) return;

        this.messageHandler.handleMessage(messageChannel, message, true, 10);
    }

    @Override
    public void handleArgs(IDiscordMessage discordMessage, String confirmKey) {
        IDiscordUser discordUser = discordMessage.getDiscordUser();
        boolean suceeded = false;

        String message = "You have successfully verified your account. You will now be able to access the plugins you have purchased, as well as the free plugins.";

        if(confirmKey.equals(discordUser.getGeneratedKey())) {
            discordUser.setVerified(this.verifyConfirmKeyContainer.getSpigotId(confirmKey));
            discordUser.addRole(Role.VERIFIED);
            suceeded = true;
        } else {
            message = "The generated key you have entered is not the same as the one you were sent, please check your Conversation to see if there is another conversation with a different key and try that one.";
        }

        this.messageHandler.handleMessage(discordUser.getPrivateMessage(), message);

        if(suceeded) {
            handlePluginVerification(discordUser);
        }
    }

    @Override
    public boolean handleWrongChannel(IDiscordMessage discordMessage) {
        boolean isHandled = !discordMessage.getMessage().getChannel().getName().equals("verify");
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);

        if(isHandled) {
            String message = "This command can only be used in the #verify channel.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
        }

        return isHandled;
    }

    @Override
    public void handlePluginVerification(IDiscordUser discordUser) {
        String message = "You have been added to the Buyers List for the following Resources:";
        StringBuilder stringBuilder = new StringBuilder();
        List<Resource> resources = null;

        try {
            resources = this.spigotPurchasedResources.getPurchasedResources(discordUser);

            for(Resource resource : resources) {
                stringBuilder.append("\n").append(" • ").append(resource.getResourceName()).append(" (by ").append(resource.getAuthor().getUsername()).append(")");
            }
        } catch (ConnectionFailedException e) {
            message = "The bot was unable to connect to SpigotSite and therefore could not get the PurchasedResources for this user.";
        }

        if(resources == null) {
            this.messageHandler.handleMessage(discordUser.getPrivateMessage(), message);
            return;
        }

        message += stringBuilder.toString();
        this.messageHandler.handleMessage(discordUser.getPrivateMessage(), message);

        //TODO: handle application of resources role
    }
}
