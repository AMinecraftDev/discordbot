package net.aminecraftdev.discordbot.commands.confirm.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public interface IConfirmModel {

    void handleNoArgs(IDiscordMessage discordMessage);

    void handleArgs(IDiscordMessage discordMessage, String confirmKey);

    boolean handleWrongChannel(IDiscordMessage discordMessage);

    void handlePluginVerification(IDiscordUser discordUser);
}
