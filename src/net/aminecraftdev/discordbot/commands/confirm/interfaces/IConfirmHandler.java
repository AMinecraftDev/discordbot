package net.aminecraftdev.discordbot.commands.confirm.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public interface IConfirmHandler extends IMessage {
}
