package net.aminecraftdev.discordbot.commands.confirm;

import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmCommand;
import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmModel;
import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmService;
import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 28-Jan-19
 */
public class ConfirmService implements IConfirmService {

    private ICommandHandler commandHandler;
    private IConfirmModel confirmModel;
    private ICommand command;

    public ConfirmService(IConfirmModel confirmModel, IConfirmCommand confirmCommand, ICommandHandler commandHandler) {
        this.command = confirmCommand;
        this.confirmModel = confirmModel;
        this.commandHandler = commandHandler;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        IDiscordUser discordUser = discordMessage.getDiscordUser();
        String[] split = rawContent.split(" ");

        if(!this.command.canAccessCommand(discordUser)) return;

        if(rawContent.equals("")) {
            this.confirmModel.handleNoArgs(discordMessage);
        } else {
            this.confirmModel.handleArgs(discordMessage, split[0]);
        }
    }
}
