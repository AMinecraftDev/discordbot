package net.aminecraftdev.discordbot.commands.confirm;

import net.aminecraftdev.discordbot.commands.confirm.interfaces.IConfirmCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-19
 */
public class ConfirmCommand implements IConfirmCommand {

    @Override
    public String getCommandDescription() {
        return "**!confirm [key]** - This will allow you to confirm the key that was sent to your account.";
    }

    @Override
    public String getPrefix() {
        return "!confirm";
    }

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return !discordUser.isVerified();
    }
}
