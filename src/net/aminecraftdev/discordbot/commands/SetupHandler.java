package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IService;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupCommand;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupHandler;
import net.aminecraftdev.discordbot.commands.setup.interfaces.ISetupService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Jun-18
 */
public class SetupHandler implements ISetupHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private ISetupService setupService;
    private ISetupCommand command;

    public SetupHandler(ISetupService setupService, ISetupCommand setupCommand, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.command = setupCommand;
        this.setupService = setupService;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.setupService;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
