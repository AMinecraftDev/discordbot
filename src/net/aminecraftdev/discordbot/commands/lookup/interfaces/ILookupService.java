package net.aminecraftdev.discordbot.commands.lookup.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public interface ILookupService extends IService {

}
