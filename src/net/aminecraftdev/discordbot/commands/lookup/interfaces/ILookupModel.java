package net.aminecraftdev.discordbot.commands.lookup.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.resource.Resource;
import net.aminecraftdev.spigot.api.user.User;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public interface ILookupModel {

    void handleNoArguments(IDiscordMessage discordMessage);

    void handleUserArgument(IDiscordMessage discordMessage, String discordId);

    List<Resource> getPremiumResources(IDiscordUser discordUser);

}
