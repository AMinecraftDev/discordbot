package net.aminecraftdev.discordbot.commands.lookup;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupCommand;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupModel;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 16-Apr-18
 */
public class LookupService implements ILookupService {

    private ICommandHandler commandHandler;
    private ILookupModel lookupModel;
    private ICommand command;

    public LookupService(ILookupModel lookupModel, ILookupCommand lookupCommand, ICommandHandler commandHandler) {
        this.lookupModel = lookupModel;
        this.command = lookupCommand;
        this.commandHandler = commandHandler;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);

        IDiscordUser discordUser = discordMessage.getDiscordUser();
        String[] args = rawContent.split(" ");

        if(!this.command.canAccessCommand(discordUser)) return;

        if(rawContent.equalsIgnoreCase("")) {
            this.lookupModel.handleNoArguments(discordMessage);
            return;
        }

        if(args[0].startsWith("<@")) {
            String discordId = args[0].replace("<@", "").replace(">", "");

            this.lookupModel.handleUserArgument(discordMessage, discordId);
        }

        //TODO: Handle lookup of SpigotName/ID
    }
}
