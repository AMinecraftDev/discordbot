package net.aminecraftdev.discordbot.commands.lookup;

import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class LookupCommand implements ILookupCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return discordUser.isStaff();
    }

    @Override
    public String getCommandDescription() {
        return "**!lookup (Discord/SpigotMC Name)** - Looks up the listed player's information.";
    }

    @Override
    public String getPrefix() {
        return "!lookup";
    }
}
