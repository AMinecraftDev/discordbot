package net.aminecraftdev.discordbot.commands.lookup;

import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.lookup.interfaces.ILookupModel;
import net.aminecraftdev.discordbot.container.interfaces.IDiscordUserContainer;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.spigot.interfaces.ISpigotPurchasedResources;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.resource.Resource;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageChannel;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 20-Apr-18
 */
public class LookupModel implements ILookupModel {

    private ISpigotPurchasedResources spigotPurchasedResources;
    private IDiscordUserContainer discordUserContainer;
    private ICommandHandler commandHandler;
    private IMessageHandler messageHandler;

    public LookupModel(IMessageHandler messageHandler, IDiscordUserContainer discordUserContainer, ISpigotPurchasedResources spigotPurchasedResources, ICommandHandler commandHandler) {
        this.messageHandler = messageHandler;
        this.commandHandler = commandHandler;
        this.discordUserContainer = discordUserContainer;
        this.spigotPurchasedResources = spigotPurchasedResources;
    }

    @Override
    public void handleNoArguments(IDiscordMessage discordMessage) {
        this.commandHandler.deleteMessage(discordMessage);
        this.messageHandler.handleEmbedMessage(
                discordMessage.getMessage().getChannel(),
                new EmbedBuilder()
                        .setColor(new Color(255, 0, 0))
                        .setTitle("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━")
                        .setFooter("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━", null)
                        .setDescription("Error! **You must enter a Discord name as well**. To use this properly you must use this format: **!lookup @DiscordName**."),
                true,
                15);
    }

    @Override
    public void handleUserArgument(IDiscordMessage discordMessage, String discordId) {
        Member member = discordMessage.getMessage().getGuild().getMemberById(discordId);
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        EmbedBuilder embedBuilder = new EmbedBuilder()
                .setTitle("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━")
                .setFooter("━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━", null);

        if(member != null) {
            IDiscordUser discordUser = this.discordUserContainer.getUser(member);

            boolean isVerified = discordUser.isVerified();
            StringBuilder description = new StringBuilder("**Lookup Information on " + member.getAsMention() + ":**\n\n");

            description.append("**Discord ID:**  ").append(discordId).append("\n");
            description.append("**Join Date:**  ").append(discordUser.getJoinDate()).append("\n");
            description.append("**Verified:**  ").append(isVerified).append("\n");

            if(isVerified) {
                description.append(" ").append("\n");
                description.append("**Verified Date:**  ").append(discordUser.getVerifiedDate()).append("\n");
                description.append("**SpigotMC ID:**  ").append(discordUser.getSpigotId()).append("\n");
                description.append("**SpigotMC Username:**  ").append(discordUser.getSpigotName()).append("\n");
                description.append("**Purchased Plugins:**  ").append(formatList(getPremiumResources(discordUser)));
                description.append(" ").append("\n");
            }

            embedBuilder.setColor(new Color(0, 255, 255))
                    .setThumbnail(member.getUser().getAvatarUrl())
                    .setDescription(description.toString());

        } else {
            embedBuilder
                    .setColor(new Color(255, 0, 0))
                    .setDescription("Error! Something went wrong while obtaining information on the targeted member.");
        }

        this.messageHandler.handleEmbedMessage(messageChannel, embedBuilder);
    }

    @Override
    public List<Resource> getPremiumResources(IDiscordUser discordUser) {
        try {
            return this.spigotPurchasedResources.getPurchasedResources(discordUser);
        } catch (ConnectionFailedException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String formatList(List<Resource> list) {
        StringBuilder stringBuilder = new StringBuilder();
        Queue<Resource> queue = new LinkedList<>(list);

        while(!queue.isEmpty()) {
            Resource resource = queue.poll();

            if(resource == null) continue;

            String authorName = resource.getAuthor().getUsername();

            stringBuilder.append("\n").append(" ► ").append(resource.getResourceName()).append(" (").append(authorName).append(")");

            if(!queue.isEmpty()) {
                stringBuilder.append("\n");
            }
        }

        return stringBuilder.toString();
    }
}
