package net.aminecraftdev.discordbot.commands;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IHandler;
import net.aminecraftdev.discordbot.commands.interfaces.IService;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginCommand;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginHandler;
import net.aminecraftdev.discordbot.commands.plugin.interfaces.IPluginService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-May-19
 */
public class PluginHandler implements IPluginHandler {

    private IHandler<IDiscordMessage> successor;
    private ICommandHandler commandHandler;
    private IService service;
    private ICommand command;

    public PluginHandler(IPluginService service, IPluginCommand command, ICommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.command = command;
        this.service = service;
    }

    @Override
    public IHandler<IDiscordMessage> getSuccessor() {
        return this.successor;
    }

    @Override
    public void setSuccessor(IHandler<IDiscordMessage> successor) {
        this.successor = successor;
    }

    @Override
    public boolean canHandle(IDiscordMessage request) {
        return this.commandHandler.canHandle(request, this.command);
    }

    @Override
    public IService getService() {
        return this.service;
    }

    @Override
    public IHandler<IDiscordMessage> handle(IDiscordMessage request) {
        return this.commandHandler.handleCommand(request, this);
    }
}
