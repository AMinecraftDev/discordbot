package net.aminecraftdev.discordbot.commands.verify;

import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyModel;
import net.aminecraftdev.discordbot.container.interfaces.ISpigotUserContainer;
import net.aminecraftdev.discordbot.container.interfaces.IVerifyConfirmKeyContainer;
import net.aminecraftdev.discordbot.database.interfaces.IDatabase;
import net.aminecraftdev.discordbot.database.users.IUserDatabaseValues;
import net.aminecraftdev.discordbot.logger.botlogs.IBotLogsLogger;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.discordbot.message.interfaces.IMessageHandler;
import net.aminecraftdev.discordbot.number.INumberHandler;
import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.exceptions.PermissionException;
import net.aminecraftdev.spigot.api.user.ConversationManager;
import net.aminecraftdev.spigot.api.user.User;
import net.aminecraftdev.spigot.api.user.UserManager;
import net.dv8tion.jda.core.entities.MessageChannel;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public class VerifyModel implements IVerifyModel {

    private IVerifyConfirmKeyContainer verifyConfirmKeyContainer;

    private ISpigotUserContainer spigotUserContainer;

    private IMessageHandler messageHandler;
    private INumberHandler numberHandler;

    private IUserDatabaseValues databaseValues;
    private IDatabase database;

    private IBotLogsLogger botLogsLogger;

    public VerifyModel(IVerifyConfirmKeyContainer verifyConfirmKeyContainer, IMessageHandler messageHandler, INumberHandler numberHandler, ISpigotUserContainer spigotUserContainer, IDatabase database, IUserDatabaseValues databaseValues, IBotLogsLogger botLogger) {
        this.messageHandler = messageHandler;
        this.numberHandler = numberHandler;

        this.verifyConfirmKeyContainer = verifyConfirmKeyContainer;

        this.spigotUserContainer = spigotUserContainer;

        this.databaseValues = databaseValues;
        this.database = database;

        this.botLogsLogger = botLogger;
    }

    @Override
    public void handleNoArgs(IDiscordMessage discordMessage) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String message = "In order to verify yourself you must include your SpigotMC name or number. A couple examples are listed below:\n" +
                         "!verify AMinecraftDev\n" +
                         "!verify 26602";

        if(handleWrongChannel(discordMessage)) return;

        this.messageHandler.handleMessage(messageChannel, message, true, 10);
    }

    @Override
    public void handleArgs(IDiscordMessage discordMessage, String spigotName) {
        if(handleWrongChannel(discordMessage)) return;
        if(handleAlreadyVerified(discordMessage, spigotName)) return;

        User foundUser = getSpigotMcUser(discordMessage, spigotName);

        if(foundUser == null) return;

        handleSpigotMCConversation(discordMessage, foundUser);
    }

    @Override
    public User getSpigotUser(String username) {
        UserManager userManager = SpigotSite.getAPI().getUserManager();

        try {
            User foundUser = userManager.getUserByName(username);

            if(foundUser == null) {
                return null;
            }

            return foundUser;
        } catch (ConnectionFailedException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getSpigotUser(int id) {
        UserManager userManager = SpigotSite.getAPI().getUserManager();

        try {
            User foundUser = userManager.getUserById(id);

            if(foundUser == null) {
                return null;
            }

            return foundUser;
        } catch (ConnectionFailedException | PermissionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean isAlreadyTaken(IDiscordMessage discordMessage, String username) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String guildId = this.messageHandler.getGuildId(discordMessage);
        String getAsMention = discordMessage.getDiscordUser().getAsMention();
        User user = getSpigotUser(username);

        if(user == null) {
            String message = "The specified user was not found.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        if(this.database.doesExist(this.databaseValues.getSpigotId(), ""+user.getUserId(), this.databaseValues)) {
            String message = "This Spigot Username has already been verified.";

            this.botLogsLogger.logMessage(getAsMention + " has tried to register the SpigotMC account " + user.getUsername() + " but that account is already connected to someone on our database.", guildId);
            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        return false;
    }

    @Override
    public boolean isAlreadyTaken(IDiscordMessage discordMessage, int id) {
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);
        String guildId = this.messageHandler.getGuildId(discordMessage);
        String getAsMention = discordMessage.getDiscordUser().getAsMention();
        User user = getSpigotUser(id);

        if(user == null) {
            String message = "The specified user was not found.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        if(this.database.doesExist(this.databaseValues.getSpigotId(), ""+id, this.databaseValues)) {
            String message = "This discord ID has already been verified.";

            this.botLogsLogger.logMessage(getAsMention + " has tried to register the SpigotMC account " + user.getUsername() + " but that account is already connected to someone on our database.", guildId);
            this.messageHandler.handleMessage(messageChannel, message, true, 10);
            return true;
        }

        return false;
    }

    @Override
    public boolean handleAlreadyVerified(IDiscordMessage discordMessage, String spigotName) {
        boolean isHandled = discordMessage.getDiscordUser().isVerified();
        boolean hasKey = discordMessage.getDiscordUser().getGeneratedKey() != null;
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);

        if(isHandled) {
            String message = "You are already verified and cannot re-verify yourself.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
        }

        if(hasKey) {
            User foundUser = getSpigotMcUser(discordMessage, spigotName);

            if(foundUser == null) return true;

            handleSpigotMCConversation(discordMessage, foundUser);
            return true;
        }

        return isHandled;
    }

    @Override
    public boolean handleWrongChannel(IDiscordMessage discordMessage) {
        boolean isHandled = !discordMessage.getMessage().getChannel().getName().equals("verify");
        MessageChannel messageChannel = this.messageHandler.getMessageChannel(discordMessage);

        if(isHandled) {
            String message = "This command can only be used in the #verify channel.";

            this.messageHandler.handleMessage(messageChannel, message, true, 10);
        }

        return isHandled;
    }

    @Override
    public User getSpigotMcUser(IDiscordMessage discordMessage, String spigotName) {
        if(this.numberHandler.isInt(spigotName)) {
            if(isAlreadyTaken(discordMessage, this.numberHandler.getInt(spigotName))) return null;
        } else {
            if(isAlreadyTaken(discordMessage, spigotName)) return null;
        }

        return this.numberHandler.isInt(spigotName)? getSpigotUser(this.numberHandler.getInt(spigotName)) : getSpigotUser(spigotName);
    }

    @Override
    public void handleSpigotMCConversation(IDiscordMessage discordMessage, User foundUser) {
        String generatedKey = this.messageHandler.generateNewCaptcha();
        MessageChannel messageChannel = discordMessage.getMessage().getChannel();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        String message = "Please check your Messages on SpigotMC and enter in the generated key and command to become verified.";

        discordMessage.getDiscordUser().setGeneratedKey(generatedKey);

        String conversationBody =
                "Hello " + foundUser.getUsername() + ",\n" +
                        "\n" +
                        "This is an automated message from the AMineriX Support Discord for you to confirm your verification on our discord server over at https://discord.gg/wAAhBk4 \n" +
                        "In order to confirm your verification you must type in [b]!confirm " + generatedKey + "[/b]\n" +
                        "\n" +
                        "If this was not you who requested this verification please start a new conversation with me and we will get back to you ASAP.\n" +
                        "\n" +
                        "Kind regard,\n" +
                        "The developers over at AMineriX.";

        User ringLeader = this.spigotUserContainer.getRingLeader();

        if(ringLeader == null) {
            message = "The ring leader has not signed in to his SpigotMC account for the bot, so you cannot be verified at this time. Please try again soon.";
            this.messageHandler.handleMessage(messageChannel, message, true, 30);
            return;
        }

        this.messageHandler.handleMessage(messageChannel, message, true, 30);

        try {
            conversationManager.createConversation(ringLeader, foundUser.getUsername(), "AMineriX Support Discord Verification", conversationBody, true, false, false);
        } catch (ConnectionFailedException e) {
            e.printStackTrace();
        }

        this.verifyConfirmKeyContainer.saveKey(generatedKey, foundUser.getUserId());
    }

}
