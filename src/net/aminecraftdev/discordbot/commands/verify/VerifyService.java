package net.aminecraftdev.discordbot.commands.verify;

import net.aminecraftdev.discordbot.commands.interfaces.ICommand;
import net.aminecraftdev.discordbot.commands.interfaces.ICommandHandler;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyCommand;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyModel;
import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyService;
import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public class VerifyService implements IVerifyService {

    private ICommandHandler commandHandler;
    private IVerifyModel verifyModel;
    private ICommand command;

    public VerifyService(IVerifyModel verifyModel, IVerifyCommand verifyCommand, ICommandHandler commandHandler) {
        this.command = verifyCommand;
        this.verifyModel = verifyModel;
        this.commandHandler = commandHandler;
    }

    @Override
    public void handleMessage(IDiscordMessage discordMessage) {
        String rawContent = this.commandHandler.getTrimmedCommand(discordMessage, this.command);
        String[] split = rawContent.split(" ");

        if(rawContent.equals("")) {
            this.verifyModel.handleNoArgs(discordMessage);
        } else {
            this.verifyModel.handleArgs(discordMessage, split[0]);
        }
    }
}
