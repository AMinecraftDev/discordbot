package net.aminecraftdev.discordbot.commands.verify;

import net.aminecraftdev.discordbot.commands.verify.interfaces.IVerifyCommand;
import net.aminecraftdev.discordbot.user.interfaces.IDiscordUser;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 27-May-19
 */
public class VerifyCommand implements IVerifyCommand {

    @Override
    public boolean canAccessCommand(IDiscordUser discordUser) {
        return !discordUser.isVerified();
    }

    @Override
    public String getCommandDescription() {
        return "**!verify [SpigotMC Name/ID]** - Send a token to the specified SpigotMC account for authentication.";
    }

    @Override
    public String getPrefix() {
        return "!verify";
    }
}
