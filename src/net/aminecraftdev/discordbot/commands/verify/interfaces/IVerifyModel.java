package net.aminecraftdev.discordbot.commands.verify.interfaces;

import net.aminecraftdev.discordbot.message.interfaces.IDiscordMessage;
import net.aminecraftdev.spigot.api.user.User;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public interface IVerifyModel {
    void handleNoArgs(IDiscordMessage discordMessage);

    void handleArgs(IDiscordMessage discordMessage, String spigotName);

    User getSpigotUser(String username);

    User getSpigotUser(int id);

    boolean isAlreadyTaken(IDiscordMessage discordMessage, String username);

    boolean isAlreadyTaken(IDiscordMessage discordMessage, int id);

    boolean handleAlreadyVerified(IDiscordMessage discordMessage, String spigotName);

    boolean handleWrongChannel(IDiscordMessage discordMessage);

    User getSpigotMcUser(IDiscordMessage discordMessage, String spigotName);

    void handleSpigotMCConversation(IDiscordMessage discordMessage, User foundUser);
}
