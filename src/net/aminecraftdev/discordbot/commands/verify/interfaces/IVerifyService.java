package net.aminecraftdev.discordbot.commands.verify.interfaces;

import net.aminecraftdev.discordbot.commands.interfaces.IService;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 12-Jun-18
 */
public interface IVerifyService extends IService {
}
