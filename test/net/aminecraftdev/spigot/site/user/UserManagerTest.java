package net.aminecraftdev.spigot.site.user;

import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.exceptions.PermissionException;
import net.aminecraftdev.spigot.api.forum.ProfilePost;
import net.aminecraftdev.spigot.api.user.User;
import net.aminecraftdev.spigot.api.user.UserManager;
import net.aminecraftdev.spigot.api.user.exceptions.InvalidCredentialsException;
import net.aminecraftdev.spigot.api.user.exceptions.TwoFactorAuthenticationException;
import net.aminecraftdev.spigot.site.SpigotSiteCore;
import net.aminecraftdev.spigot.site.UserDebugging;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class UserManagerTest {

    @Before
    public void init() {
        new SpigotSiteCore();
    }

    @Test(timeout = 15000)
    public void getUsernamesByNameTest() throws ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'getUsernamesByName' ...");
        UserManager userManager = SpigotSite.getAPI().getUserManager();
        List<String> users = userManager.getUsernamesByName("AMine");
        System.out.println("Found: ");
        for (String user : users){
            System.out.println("\t" + user);
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test
    public void logOutTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'Logout' ...");
        SpigotUserManager userManager = ((SpigotUserManager) SpigotSite.getAPI().getUserManager());
        User user = UserDebugging.getUser();
        assertTrue(userManager.isLoggedIn(user));
        userManager.logOff(user);
        assertFalse(userManager.isLoggedIn(user));
        user = userManager.authenticate(UserDebugging.username,UserDebugging.password,user);
        assertTrue(userManager.isLoggedIn(user));
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 15000)
    public void getUserByNameTest() throws ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'getUserByName' ...");
        UserManager userManager = SpigotSite.getAPI().getUserManager();
        User u1 = userManager.getUserByName("JamesJ");
        System.out.println("Found (JamesJ): " + u1.getUserId());
        assertEquals(8614,u1.getUserId());
        User u2 = userManager.getUserByName("clip");
        System.out.println("Found (clip): " + u2.getUserId());
        assertEquals(1001,u2.getUserId());
        User u3 = userManager.getUserByName("$#$G#$");
        assertNull(u3);
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 15000)
    public void getUserByIdTest() throws ConnectionFailedException, PermissionException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'getUserById 1709' ...");
        UserManager userManager = SpigotSite.getAPI().getUserManager();
        User user = userManager.getUserById(1709);
        System.out.println("Username: " + user.getUsername());
        System.out.println("User Id: " + user.getUserId());
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 15000)
    public void getUserActivityTest() throws ConnectionFailedException, PermissionException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'getUserActivityTest' ...");
        UserManager userManager = SpigotSite.getAPI().getUserManager();
        User user = userManager.getUserById(26602);
        System.out.println("Username: " + user.getUsername());
        System.out.println("User Id: " + user.getUserId());
        System.out.println("Activity: " + user.getLastActivity());
        System.out.println("--------------------------------------------------------------");
    }

    @Test
    public void untrustTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        User user = UserDebugging.getUser();
        SpigotUserManager userManager = ((SpigotUserManager) SpigotSite.getAPI().getUserManager());
        userManager.untrustThisDevice(user);
        assertTrue(userManager.isLoggedIn(user));
        System.out.println("--------------------------------------------------------------");
    }

    public void getUsersByRankTest() {

    }

    public void getUserRanksTest() {

    }

    @Test
    public void getProfilePostsTest() throws TwoFactorAuthenticationException, ConnectionFailedException, InvalidCredentialsException {
        System.out.println("--------------------------------------------------------------");
        User user = UserDebugging.getUser();
        SpigotUserManager userManager = ((SpigotUserManager) SpigotSite.getAPI().getUserManager());
        List<ProfilePost> posts = userManager.getProfilePosts(user,user,10);
        for (ProfilePost post : posts){
            System.out.println(post.getAuthor().getUsername() + ": " + post.getMessage());
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 60000)
    public void logInUserTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'authenticate' ...");
        UserManager userManager = SpigotSite.getAPI().getUserManager();
        User user = userManager.authenticate(UserDebugging.username, UserDebugging.password, UserDebugging.totpSecret);
        assertEquals(user.getUsername(), "AMinecraftDev");
        System.out.println("Logged in: " + user.getUsername() + " [" + user.getUserId() + "]");
        if (user.hasTwoFactorAuthentication()){
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("--------------------------------------------------------------");
    }
}