package net.aminecraftdev.spigot.site.user;

import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.exceptions.SpamWarningException;
import net.aminecraftdev.spigot.api.user.Conversation;
import net.aminecraftdev.spigot.api.user.ConversationManager;
import net.aminecraftdev.spigot.api.user.User;
import net.aminecraftdev.spigot.api.user.exceptions.InvalidCredentialsException;
import net.aminecraftdev.spigot.api.user.exceptions.TwoFactorAuthenticationException;
import net.aminecraftdev.spigot.site.SpigotSiteCore;
import net.aminecraftdev.spigot.site.UserDebugging;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConversationManagerTest {

    @Before
    public void init() {
        new SpigotSiteCore();
    }

    @Test
    public void conversationsTest() throws InvalidCredentialsException,
            ConnectionFailedException, TwoFactorAuthenticationException, InterruptedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'getConversations' ...");
        User user = UserDebugging.getUser();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        List<Conversation> conversations = conversationManager.getConversations(user, 10000);

        for (Conversation conv : conversations) {
            System.out.println(conv.getTitle() + " [" + conv.getRepliesCount() + "] BY " + conv.getAuthor().getUsername());
            if (conv.getTitle().equals("Hello") && conv.getAuthor().getUsername().equals("AMinecraftDev") && conv.getParticipants().size() == 1) {
                if (conv.getParticipants().get(0).getUsername().equals("BoostedNetwork")) {
                    Thread.sleep(12000);
                    System.out.println("Sending reply ...");
                    conv.reply(user, "This conversation has " + conv.getRepliesCount() + " replies. LEAVING NOW");
                    conv.leave(user);
                }
            }
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test
    public void markAllAsReadTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'mark all as read conversation' ...");
        User user = UserDebugging.getUser();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        List<Conversation> conversations = conversationManager.getConversations(user, 10000);

        for (Conversation conv : conversations) {
            if (conv.isUnread()) {
                System.out.println("Unread conversation: " + conv.getTitle() + " by " + conv.getAuthor().getUsername());
                conv.markAsRead(user);
            }
        }
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 30000, expected = SpamWarningException.class)
    public void spamConversationTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'Spam detection' ...");
        User user = UserDebugging.getUser();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        Set<String> recipents = new HashSet<>();
        recipents.add("BoostedNetwork");
        conversationManager.createConversation(user, recipents, "Hello", "World", true, false, false);
        conversationManager.createConversation(user, recipents, "Hello", "World", true, false, false);
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 20000)
    public void conversationSendTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'createConversation' ...");
        User user = UserDebugging.getUser();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        Set<String> recipents = new HashSet<>();
        recipents.add("Splodgebox");
        try {
            conversationManager.createConversation(user, recipents, "Hello", "World", true, false, false);
        } catch (SpamWarningException ex) {

        }

        SpigotUser spigotUser = (SpigotUser) user;

        for (String cookie : spigotUser.getCookies().keySet()) {
            System.out.println("Return cookie: " + cookie);
        }

        spigotUser.refresh();
        System.out.println("--------------------------------------------------------------");
    }

    @Test(timeout = 20000)
    public void conversationSendMarkReadTest() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        System.out.println("--------------------------------------------------------------");
        System.out.println("Testing 'conversationSendMarkReadTest' ...");
        User user = UserDebugging.getUser();
        ConversationManager conversationManager = SpigotSite.getAPI().getConversationManager();
        Set<String> recipents = new HashSet<>();
        recipents.add("BoostedNetwork");
        try {
            Conversation conversation = conversationManager.createConversation(user, recipents, "Hello", "World", true, false, false);
            conversation.markAsUnread(user);
        } catch (SpamWarningException ex) {

        }

        SpigotUser spigotUser = (SpigotUser) user;
        for (String cookie : spigotUser.getCookies().keySet())
            System.out.println("Return cookie: " + cookie);

        spigotUser.refresh();
        System.out.println("--------------------------------------------------------------");
    }
}