package net.aminecraftdev.spigot.site;

import net.aminecraftdev.spigot.api.SpigotSite;
import net.aminecraftdev.spigot.api.exceptions.ConnectionFailedException;
import net.aminecraftdev.spigot.api.user.User;
import net.aminecraftdev.spigot.api.user.UserManager;
import net.aminecraftdev.spigot.api.user.exceptions.InvalidCredentialsException;
import net.aminecraftdev.spigot.api.user.exceptions.TwoFactorAuthenticationException;

import java.io.*;

/**
 * UserDebugging
 * <p>
 * Created by maxim on 30-Nov-16.
 */
public class UserDebugging {
    public static String username = "";
    public static String password = "";
    public static String totpSecret = "";
    private static User user = null;

    static {
        BufferedReader br = null;
        try {
            if (new File("C:\\GIT Projects\\discordbot\\credentials.txt").exists()) {
                br = new BufferedReader(new FileReader("C:\\GIT Projects\\discordbot\\credentials.txt"));
            } else if (new File("/opt/aminerix/credentials.txt").exists()) {
                br = new BufferedReader(new FileReader("/opt/aminerix/credentials.txt"));
            }

            if(br != null) {
                username = br.readLine();
                password = br.readLine();
                totpSecret = br.readLine();
            } else {
                System.out.println("BufferedReader didn't find a credentials file!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static User getUser() throws InvalidCredentialsException, TwoFactorAuthenticationException, ConnectionFailedException {
        if (user != null) return user;

        UserManager userManager = SpigotSite.getAPI().getUserManager();

        user = userManager.authenticate(UserDebugging.username, UserDebugging.password,UserDebugging.totpSecret);

        return user;
    }
}